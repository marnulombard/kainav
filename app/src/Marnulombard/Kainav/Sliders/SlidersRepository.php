<?php namespace Marnulombard\Kainav\Sliders;

use Marnulombard\Kainav\Core\EloquentBaseRepository;
use Marnulombard\Kainav\Abstracts\Traits\TaggableRepository;

class SlidersRepository extends EloquentBaseRepository implements SlidersInterface
{

    use TaggableRepository;

    /**
     * Construct Shit
     * @param Sliders $sliders
     * @internal param \Marnulombard\Kainav\Sliders\Sliders $sliders
     */
    public function __construct( Sliders $sliders )
    {
        $this->model = $sliders;
    }

    /**
     * Get all sliders by date published ascending
     * @return Sliders
     */
    public function getAllByDateAsc()
    {
        return $this->model->orderBy('created_at','asc')->get();
    }

    /**
     * Get all sliders by date published descending
     * @return Sliders
     */
    public function getAllByDateDesc()
    {
        return $this->model->orderBy('created_at','desc')->get();
    }

}
