<?php namespace Marnulombard\Kainav\Sliders;

interface SlidersInterface {

    /**
     * Get all pages by date published ascending
     * @return Pages
     */
    public function getAllByDateAsc();

    /**
     * Get all pages by date published descending
     * @return Pages
     */
    public function getAllByDateDesc();

}
