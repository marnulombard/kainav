<?php namespace Marnulombard\Kainav\Sliders;

use Marnulombard\Kainav\Core\EloquentBaseModel;
use Marnulombard\Kainav\Abstracts\Traits\TaggableRelationship;
use Marnulombard\Kainav\Abstracts\Traits\UploadableRelationship;
use Str, Input;

class Sliders extends EloquentBaseModel
{

    use TaggableRelationship; // Enable The Tags Relationships
    use UploadableRelationship; // Enable The Uploads Relationships

    /**
     * The table to get the data from
     * @var string
     */
    protected $table    = 'sliders';

    /**
     * These are the mass-assignable keys
     * @var array
     */
    protected $fillable = array('title', 'content', 'on_page',);

    protected $validationRules = [
        'title'     => 'required',
        'content'     => 'required',
        'on_page'  => 'required',
    ];

    /**
     * Fill the model up like we usually do but also allow us to fill some custom stuff
     * @param array $attributes
     * @internal param array $array The array of data, this is usually Input::all();
     * @return void
     */
    public function fill( array $attributes )
    {
        parent::fill( $attributes );
    }

}
