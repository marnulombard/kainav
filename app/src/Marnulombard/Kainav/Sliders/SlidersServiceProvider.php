<?php namespace Marnulombard\Kainav\Sliders;

use Marnulombard\Kainav\Sliders\Sliders;
use Marnulombard\Kainav\Sliders\SlidersRepository;
use Illuminate\Support\ServiceProvider;

class SlidersServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->app->bind('Marnulombard\Kainav\Sliders\SlidersInterface', function()
    {
      $sliders = new Sliders();
      $slidersRepository = new SlidersRepository($sliders);

      return $slidersRepository;

    });
  }
} 
