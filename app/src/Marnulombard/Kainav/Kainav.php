<?php

/**
 * ======================================================================================
 * PLEASE NOTE::
 * This package may have been renamed, but it is a virtual clone of Davzie\LaravelBootstrap
 * All credit to the original Author; Not MarnuLombard
 * ======================================================================================
 */



namespace Marnulombard\Kainav;

/**
* This is the Base class providing the CMS for the project
*/
class Kainav
{

  /**
   * Register the class with Laravel’s IoC container
   * @return injection
   */
  public function register()
  {
    $this->app['kainav'] = $this->app->share(function($app)
    {
      return new Kainav;
    });
  }
}
