<?php

//-----------------------------------------------------------------
// Admin routes

// Get the URL segment to use for routing
$urlSegment = Config::get('kainav::app.access_url');

// Filter all requests ensuring a user is logged in when this filter is called
Route::filter('adminFilter', 'Marnulombard\Kainav\Filters\Admin');

Route::controller( $urlSegment.'/blog'    , 'Marnulombard\Kainav\Controllers\BlogController' );
Route::controller( $urlSegment.'/news'     , 'Marnulombard\Kainav\Controllers\NewsController' );
Route::controller( $urlSegment.'/pages'     , 'Marnulombard\Kainav\Controllers\PagesController' );
Route::controller( $urlSegment.'/projects'     , 'Marnulombard\Kainav\Controllers\ProjectsController' );
Route::controller( $urlSegment.'/settings'  , 'Marnulombard\Kainav\Controllers\SettingsController' );
Route::controller( $urlSegment.'/sponsors'  , 'Marnulombard\Kainav\Controllers\SponsorsController' );
Route::controller( $urlSegment.'/sliders'    , 'Marnulombard\Kainav\Controllers\SlidersController' );
Route::controller( $urlSegment.'/team_members' , 'Marnulombard\Kainav\Controllers\TeamMembersController' );
Route::controller( $urlSegment.'/users'     , 'Marnulombard\Kainav\Controllers\UsersController' );
Route::controller( $urlSegment              , 'Marnulombard\Kainav\Controllers\DashController'  );
//=================================================================


