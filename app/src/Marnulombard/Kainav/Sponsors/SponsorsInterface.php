<?php namespace Marnulombard\Kainav\Sponsors;

interface SponsorsInterface {

  /**
   * Return by the 'type' column
   *
   * @param string $type
   * @return Eloquent
   */
  public function getByType($type);

  /**
   * Return X records of 'type' column
   *
   * @param integer $number
   * @param string $type
   *
   * @return Eloquent
   */
  public function getOnlyXOfType($number, $type);

}
