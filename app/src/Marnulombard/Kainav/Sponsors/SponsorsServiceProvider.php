<?php namespace Marnulombard\Kainav\Sponsors;

use Marnulombard\Kainav\Sponsors\Sponsors;
use Marnulombard\Kainav\Sponsors\SponsorsRepository;
use ServiceProvider;

class SponsorsServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->app->bind('Marnulombard\Kainav\Sponsors\SponsorsInterface', function()
    {
      $sponsors = new Sponsors;
      $sponsorsRepository = new SponsorsRepository($sponsors);

      return $sponsorsRepository->getAllByDateAsc();

    });
  }
} 
