<?php namespace Marnulombard\Kainav\Sponsors;

use Marnulombard\Kainav\Core\EloquentBaseRepository;
use Marnulombard\Kainav\Abstracts\Traits\TaggableRepository;
use Marnulombard\Kainav\Abstracts\Traits\UploadableRepository;

class SponsorsRepository extends EloquentBaseRepository implements SponsorsInterface
{
  use TaggableRepository;
  use UploadableRepository;


  /**
   * Construct Shit
   *
   * @param Sponsors $sponsors
   *
   */
  public function __construct( Sponsors $sponsors )
  {
    $this->model = $sponsors;
  }

  /**
   * Return by the 'type' column
   *
   * @param string $type
   *
   * @return Eloquent
   */
  public function getByType($type)
  {
    return $this->model->where('type', '=', $type)->get();
  }

  /**
   * Return X records of 'type' column
   *
   * @param integer $number
   * @param string  $type
   *
   * @return Eloquent
   */
  public function getOnlyXOfType($number, $type)
  {
    $result = $this->model->where('type', '=', $type)->get();
    return $result->take($number)->all();
  }
}
