<?php namespace Marnulombard\Kainav\News;

interface NewsInterface {
  /**
   * Get N Most recent Items from the db
   * Will move over to BaseRepository if needed
   *
   * @param integer $number
   */
  public function getMostRecent($number);


  /**
   * Return those that we need for home page
   *
   * @param array $sections
   * @return \Eloquent
   */
  public function getHomePageItems($sections);
}
