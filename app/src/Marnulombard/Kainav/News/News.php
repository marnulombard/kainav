<?php namespace Marnulombard\Kainav\News;

use Marnulombard\Kainav\Abstracts\Traits\UploadableRelationship;
use Marnulombard\Kainav\Core\EloquentBaseModel;
use Str, Input;

class News extends EloquentBaseModel
{

  use UploadableRelationship;

    /**
     * The table to get the data from
     * @var string
     */
    protected $table    = 'news';

    /**
     * These are the mass-assignable keys
     * @var array
     */
    protected $fillable = array('title', 'content', 'type', 'link');

    protected $validationRules = [
        'title'     => 'required',
        'content'     => 'required',
    ];

    /**
     * Fill the model up like we usually do but also allow us to fill some custom stuff
     * @param array $attributes
     * @internal param array $array The array of data, this is usually Input::all();
     * @return void
     */
    public function fill( array $attributes )
    {
        parent::fill( $attributes );
    }

}
