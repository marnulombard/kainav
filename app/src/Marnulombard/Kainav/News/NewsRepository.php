<?php namespace Marnulombard\Kainav\News;

use Marnulombard\Kainav\Abstracts\Traits\UploadableRepository;
use Marnulombard\Kainav\Core\EloquentBaseRepository;
use Illuminate\Database\Eloquent\Collection;
use URL;

class NewsRepository extends EloquentBaseRepository implements NewsInterface
{

  use UploadableRepository;

  /**
   * Construct Shit
   *
   * @param News $news
   *
   */
    public function __construct( News $news )
    {
        $this->model = $news;
    }

  /**
   * Get N Most recent Items from the db
   * Will move over to BaseRepository if needed
   *
   * @param integer $number
   */
  public function getMostRecent($number)
  {
    return $this->model->orderBy('id', 'dec')
      ->take($number)
      ->get();
  }


  /**
   * Return those that we need for home page
   *
   * @param array $sections
   * @return \Eloquent
   */
  public function getHomePageItems($sections)
  {
    $types = new Collection();
    foreach($sections as $section)
    {
      $sectionModel = $this->model->where('type', '=', $section)->first();
      $images = [];
      foreach($sectionModel->uploads as $upload)
      {
        $images[] = $upload->sizeImg(180, 140, true);
      }

      $types->push((object)[
        'title' => $sectionModel->title,
        'images' => $images,
        'type' => $sectionModel->type,
        'content' => $sectionModel->content,
        'link' => $sectionModel->link,
      ]);
    }

    return $types;
  }

}
