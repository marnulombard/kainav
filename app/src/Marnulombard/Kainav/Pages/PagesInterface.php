<?php namespace Marnulombard\Kainav\Pages;

interface PagesInterface {

  /**
   * All Links
   * @return Eloquent
   */
  public function getAllTopLevelItems();

  /**
   * All DropDowns
   * @return Eloquent
   */
  public function getAllDropDowns();

  /**
   * All Items in the dropdowns
   * @return Eloquent
   */

  public function getAllSubItems();

}
