<?php namespace Marnulombard\Kainav\Pages;

use Marnulombard\Kainav\Core\EloquentBaseModel;
use Marnulombard\Kainav\Abstracts\Traits\TaggableRelationship;
use Marnulombard\Kainav\Abstracts\Traits\UploadableRelationship;
use Str, Input;

class Pages extends EloquentBaseModel
{

    use TaggableRelationship; // Enable The Tags Relationships
    use UploadableRelationship; // Enable The Uploads Relationships

    /**
     * The table to get the data from
     * @var string
     */
    protected $table    = 'pages';

    /**
     * These are the mass-assignable keys
     * @var array
     */

    protected $fillable = array('title', 'slug', 'launched', 'type', 'linked_to', 'content');

    protected $validationRules = [
      'title' => 'required',
      'type' => 'required',
      'launched' => 'required|max:1',
      'linked_to' => 'required_if:type,subLink',
      'content' => '',
    ];

    /**
     * Fill the model up like we usually do but also allow us to fill some custom stuff
     * @param array $attributes
     * @internal param array $array The array of data, this is usually Input::all();
     * @return void
     */
    public function fill( array $attributes )
    {
      parent::fill( $attributes );
      $this->slug = Str::slug( $this->title , '-' );
      $this->linked_to = Str::slug( $this->linked_to , '-' );
    }

}
