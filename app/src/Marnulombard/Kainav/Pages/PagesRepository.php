<?php namespace Marnulombard\Kainav\Pages;

use Marnulombard\Kainav\Core\EloquentBaseRepository;
use Marnulombard\Kainav\Abstracts\Traits\TaggableRepository;

class PagesRepository extends EloquentBaseRepository implements PagesInterface
{
    use TaggableRepository;


  /**
   * Construct Shit
   *
   * @param Pages $Pages
   *
   */
    public function __construct( Pages $Pages )
    {
        $this->model = $Pages;
    }

  /**
   * All Links
   * @return Eloquent
   */

  public function getAllTopLevelItems()
  {
    return $this->model->where('type', '=', 'link');
  }

  /**
   * All DropDowns
   * @return Eloquent
   */
  public function getAllDropDowns()
  {
    return $this->model->where('type', '=', 'dropDrown');
  }

  /**
   * All Items in the dropdowns
   * @return Eloquent
   */
  public function getAllSubItems()
  {
    return $this->model->where('type', '=', 'subLink');
  }
}
