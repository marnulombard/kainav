<?php namespace Marnulombard\Kainav\Pages;

use Marnulombard\Kainav\Pages\Pages;
use Marnulombard\Kainav\Pages\PagesRepository;
use ServiceProvider;

class PagesServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->app->bind('Marnulombard\Kainav\Pages\PagesInterface', function()
    {
      $pages = new Pages;
      $pagesRepository = new PagesRepository($pages);

      return $pagesRepository->getAllByDateAsc();

    });
  }
} 
