<?php  namespace Marnulombard\Kainav\TeamMembers;

use Marnulombard\Kainav\TeamMembers\TeamMembers;
use Marnulombard\Kainav\TeamMembers\TeamMembersRepository;

class TeamMembersServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->app->bind('teamMembers', function()
    {
      $teamMembers = new TeamMembers;
      $teamMembersRepository = new TeamMembersRepository($teamMembers);

      return $teamMembers->getAll();

    });
  }
} 
