<?php namespace Marnulombard\Kainav\TeamMembers;

interface TeamMembersInterface {

    /**
     * Get all team_members team_members by date published ascending
     * @return TeamMembers
     */
    public function getAllByDateAsc();

    /**
     * Get all team_members by date published descending
     * @return TeamMembers
     */
    public function getAllByDateDesc();

}
