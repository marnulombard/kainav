<?php namespace Marnulombard\Kainav\TeamMembers;

use Marnulombard\Kainav\TeamMembers\TeamMembers;
use Marnulombard\Kainav\Core\EloquentBaseRepository;
use Marnulombard\Kainav\Abstracts\Traits\TaggableRepository;

class TeamMembersRepository extends EloquentBaseRepository implements TeamMembersInterface
{
    use TaggableRepository;


  /**
   * Construct Shit
   *
   * @param TeamMembers $teamMembers
   */
    public function __construct(TeamMembers $teamMembers )
    {
        $this->model = $teamMembers;
    }

    /**
     * Get all team_members by date published ascending
     * @return TeamMembers
     */
    public function getAllByDateAsc()
    {
        return $this->model->orderBy('created_at','asc')->get();
    }

    /**
     * Get all team_members by date published descending
     * @return TeamMembers
     */
    public function getAllByDateDesc()
    {
        return $this->model->orderBy('created_at','desc')->get();
    }

}
