<?php

namespace Marnulombard\Kainav\Blog;

use Marnulombard\Kainav\Core\EloquentBaseRepository;
use Marnulombard\Kainav\Abstracts\Traits\TaggableRepository;

class BlogRepository extends EloquentBaseRepository implements BlogInterface
{

    use TaggableRepository;

    /**
     * Construct Shit
     * @param Blog $blog
     *
     * @internal param \Marnulombard\Kainav\Blog\Blog $blog
     */
    public function __construct( Blog $blog )
    {
        $this->model = $blog;
    }

}
