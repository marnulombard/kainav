<?php  namespace Marnulombard\Kainav\Blog;

use Marnulombard\Kainav\Blog\Blog;
use Marnulombard\Kainav\Blog\BlogRepository;

class BlogServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->app->bind('blog', function()
    {
      $blog = new Blog;
      $blogRepository = new BlogRepository($blog);

      return $blogRepository->getAllByDateAsc();

    });
  }
} 
