<?php 

namespace Marnulombard\Kainav\Blog;

use Marnulombard\Kainav\Core\EloquentBaseModel;
use Marnulombard\Kainav\Abstracts\Traits\TaggableRelationship;
use Marnulombard\Kainav\Abstracts\Traits\UploadableRelationship;
use Str, Input;

class Blog extends EloquentBaseModel
{

  use TaggableRelationship; // Enable The Tags Relationships
  use UploadableRelationship; // Enable The Uploads Relationships

  /**
   * The table to get the data from
   * @var string
   */
  protected $table    = 'blog';

  /**
   * These are the mass-assignable keys
   * @var array
   */
  protected $fillable = array('title', 'slug', 'content');

  protected $validationRules = [
    'title'     => 'required',
    'slug'      => 'required|unique:blog,id,<id>',
    'content'   => 'required'
  ];

  /**
   * Fill the model up like we usually do but also allow us to fill some custom stuff
   * @param array $attributes
   * @internal param array $array The array of data, this is usually Input::all();
   * @return void
   */
  public function fill( array $attributes )
  {
    parent::fill( $attributes );
    $this->slug = Str::slug( $this->title , '-' );
  }

}
