<?php

namespace Marnulombard\Kainav\Blog;

interface BlogInterface {

    /**
     * Get all the blog where the key is equal to the item you mention
     * @param $key
     * @return Eloquent
     */
    public function getByKey($key);

}
