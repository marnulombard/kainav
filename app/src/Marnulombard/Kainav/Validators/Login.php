<?php namespace Marnulombard\Kainav\Validators;
use Marnulombard\Kainav\Abstracts\ValidatorBase;

class Login extends ValidatorBase
{

    protected $rules = array(
        'email'         =>  'required|email',
        'password'      =>  'required'
    );

}
