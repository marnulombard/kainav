<?php namespace Marnulombard\Kainav\Projects;

use Marnulombard\Kainav\Projects\Projects;
use Marnulombard\Kainav\Projects\ProjectsRepository;
use ServiceProvider;

class ProjectsServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->app->bind('Marnulombard\Kainav\Projects\ProjectsInterface', function()
    {
      $projects = new Projects;
      $projectsRepository = new ProjectsRepository($projects);

      return $projectsRepository->getAllByDateAsc();

    });
  }
} 
