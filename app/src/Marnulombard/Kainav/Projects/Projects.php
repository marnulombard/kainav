<?php namespace Marnulombard\Kainav\Projects;

use Marnulombard\Kainav\Core\EloquentBaseModel;
use Marnulombard\Kainav\Abstracts\Traits\TaggableRelationship;
use Marnulombard\Kainav\Abstracts\Traits\UploadableRelationship;
use Str, Input;

class Projects extends EloquentBaseModel
{

    use UploadableRelationship; // Enable The Uploads Relationships

    /**
     * The table to get the data from
     * @var string
     */
    protected $table    = 'projects';

    /**
     * These are the mass-assignable keys
     * @var array
     */

    protected $fillable = array('title', 'category', 'year', 'caption' , 'content' , 'key',);

    protected $validationRules = [
      'title' => 'required',
      'category' => 'required',
      'year' => 'required|numeric',
      'caption' => 'required',
      'content' => 'required',
      'key' => '',
    ];

    /**
     * Fill the model up like we usually do but also allow us to fill some custom stuff
     * @param array $attributes
     * @internal param array $array The array of data, this is usually Input::all();
     * @return void
     */
    public function fill( array $attributes )
    {
        parent::fill( $attributes );
        $this->key = Str::slug( $this->title , '-' );
    }

}
