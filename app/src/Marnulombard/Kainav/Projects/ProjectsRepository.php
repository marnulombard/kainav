<?php namespace Marnulombard\Kainav\Projects;

use Marnulombard\Kainav\Projects\Projects;
use Marnulombard\Kainav\Core\EloquentBaseRepository;
use Marnulombard\Kainav\Abstracts\Traits\TaggableRepository;

class ProjectsRepository extends EloquentBaseRepository implements ProjectsInterface
{
    use TaggableRepository;


  /**
   * Construct Shit
   *
   * @param Projects $projects
   *
   * @internal param \Marnulombard\Kainav\Projects\Projects $Projects
   */
    public function __construct( Projects $projects )
    {
        $this->model = $projects;
    }

  /**
   * Get all Projects tagged as personal
   */
  public function getAllPersonal()
  {
    return $this->model->where('category', '=', 'personal')->get();
  }

  /**
   * Get all Projects tagged as programmes
   */
  public function getAllProgrammes()
  {
    return $this->model->where('category', '=', 'programme')->get();
  }
}
