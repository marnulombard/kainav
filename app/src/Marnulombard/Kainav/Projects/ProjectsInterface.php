<?php namespace Marnulombard\Kainav\Projects;

interface ProjectsInterface {

  /**
   * Get all Projects tagged as personal
   */
  public function getAllPersonal();

  /**
   * Get all Projects tagged as programmes
   */
  public function getAllProgrammes();

}
