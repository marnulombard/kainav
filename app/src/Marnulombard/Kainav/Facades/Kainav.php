<?php

namespace Marnulombard\Kainav\Facades;

use Illuminate\Support\Facades\Facade;

class Kainav extends Facade
{
  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getFacadeAccessor() { return 'kainav'; }
}
