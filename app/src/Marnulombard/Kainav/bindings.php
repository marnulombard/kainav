<?php


//--------------------------------------
// Interface Bindings
//======================================

// The Blog Bindings
App::bind('Marnulombard\Kainav\Blog\BlogInterface', function(){
    return new Marnulombard\Kainav\Blog\BlogRepository( new Marnulombard\Kainav\Blog\Blog );
});

// The News Bindings
App::bind('Marnulombard\Kainav\News\NewsInterface', function(){
  return new Marnulombard\Kainav\News\NewsRepository( new Marnulombard\Kainav\News\News);
});

// The Pages Bindings
App::bind('Marnulombard\Kainav\Pages\PagesInterface', function(){
  return new Marnulombard\Kainav\Pages\PagesRepository( new Marnulombard\Kainav\Pages\Pages);
});

// The Projects Bindings
App::bind('Marnulombard\Kainav\Projects\ProjectsInterface', function(){
  return new Marnulombard\Kainav\Projects\ProjectsRepository( new Marnulombard\Kainav\Projects\Projects);
});

// The Settings Bindings
App::bind('Marnulombard\Kainav\Settings\SettingsInterface', function(){
    return new Marnulombard\Kainav\Settings\SettingsRepository( new Marnulombard\Kainav\Settings\Settings );
});

// The Sliders Bindings
App::bind('Marnulombard\Kainav\Sliders\SlidersInterface', function(){
  return new Marnulombard\Kainav\Sliders\SlidersRepository( new Marnulombard\Kainav\Sliders\Sliders);
});

// The Sponsors Bindings
App::bind('Marnulombard\Kainav\Sponsors\SponsorsInterface', function(){
  return new Marnulombard\Kainav\Sponsors\SponsorsRepository( new Marnulombard\Kainav\Sponsors\Sponsors);
});

// The TeamMembers Bindings
App::bind('Marnulombard\Kainav\TeamMembers\TeamMembersInterface', function(){
  return new Marnulombard\Kainav\TeamMembers\TeamMembersRepository( new Marnulombard\Kainav\TeamMembers\TeamMembers);
});

// The Tags Bindings
App::bind('Marnulombard\Kainav\Tags\TagsInterface', function(){
    return new Marnulombard\Kainav\Tags\TagsRepository( new Marnulombard\Kainav\Tags\Tags );
});

// The Uploads Bindings
App::bind('Marnulombard\Kainav\Uploads\UploadsInterface', function(){
    return new Marnulombard\Kainav\Uploads\UploadsRepository( new Marnulombard\Kainav\Uploads\Uploads );
});

// The Users Bindings
App::bind('Marnulombard\Kainav\Accounts\UserInterface', function(){
    return new Marnulombard\Kainav\Accounts\UserRepository( new Marnulombard\Kainav\Accounts\User );
});
