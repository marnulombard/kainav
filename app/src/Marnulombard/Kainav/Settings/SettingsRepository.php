<?php namespace Marnulombard\Kainav\Settings;
use Marnulombard\Kainav\Core\EloquentBaseRepository;
use Config;

class SettingsRepository extends EloquentBaseRepository implements SettingsInterface
{

    /**
     * Construct Shit
     * @param Settings $settings
     */
    public function __construct( Settings $settings )
    {
        $this->model = $settings;
    }

    /**
     * Bit of a unique one here, get the application name from the database
     * If it doesn't exist in the key we expect then use the fallback configuration file
     * @return string
     */
    public function getAppName()
    {
        if( $name = $this->model->where('key','application_name')->first() )
            return $name->value;

        return Config::get('kainav::app.name');
    }

}
