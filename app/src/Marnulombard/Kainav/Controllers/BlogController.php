<?php

namespace Marnulombard\Kainav\Controllers;

use Marnulombard\Kainav\Blog\BlogInterface;

class BlogController extends ObjectBaseController {

    /**
     * The place to find the views / URL keys for this controller
     * @var string
     */
    protected $view_key = 'blog';

    /**
     * Construct Shit
     */
    public function __construct( BlogInterface $blog )
    {
        $this->model = $blog;
        parent::__construct();
    }

}
