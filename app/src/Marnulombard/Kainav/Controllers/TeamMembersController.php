<?php

namespace Marnulombard\Kainav\Controllers;

use Marnulombard\Kainav\TeamMembers\TeamMembersInterface;
use Input, Redirect, Str, View;
use Illuminate\Support\MessageBag;

class TeamMembersController extends ObjectBaseController {

    /**
     * The place to find the views / URL keys for this controller
     * @var string
     */
    protected $view_key = 'team_members';

    /**
     * Construct Shit
     */
    public function __construct( TeamMembersInterface $teamMembers )
    {
        $this->model = $teamMembers;
        parent::__construct();
    }
  
    /**
     * The method to handle the posted data
     * @param  integer $id The ID of the object
     * @return Redirect
     */
    public function pagesEdit( $id )
    {
        $record = $this->model->requireById( $id );
        $record->fill( Input::all() );

        if( !$record->isValid() )
            return Redirect::to( $this->edit_url.$id )->with( 'errors' , $record->getErrors() );

        // Run the hydration method that populates anything else that is required / runs any other
        // model interactions and save it.
        $record->hydrateRelations()->save();

        // Redirect that shit man! You did good! Validated and saved, man mum would be proud!
        return Redirect::to( $this->object_url )->with( 'success' , new MessageBag( array( 'Item Saved' ) ) );
    }

}
