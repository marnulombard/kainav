<?php

namespace Marnulombard\Kainav\Controllers;

use Marnulombard\Kainav\Pages\PagesInterface;
use Marnulombard\Kainav\Sliders\SlidersInterface;
use Illuminate\Support\MessageBag;
use Input, Redirect, Str, View;


class SlidersController extends ObjectBaseController {

    /**
     * The place to find the views / URL keys for this controller
     * @var string
     */
    protected $view_key = 'sliders';

  /**
   * The pages
   */
  protected $pages;

  /**
     * Construct Shit
     */
    public function __construct( SlidersInterface $sliders, PagesInterface $pagesInterface )
    {
      $this->model = $sliders;
      $this->pages = $pagesInterface;

      parent::__construct();
    }

  /**
   * The new object
   * @access public
   * @return View
   */
  public function getNew(){
    $nextItemsId = $this->model->getNewestId();
    $allPages = $this->pages->getAll();

    if( !View::exists( 'kainav::'.$this->view_key.'.new' ) )
      return App::abort(404, 'Page not found');

    return View::make('kainav::'.$this->view_key.'.new')
      ->with('pages', $allPages)
      ->with('id', $nextItemsId);
  }

  /**
   * The generic method for the start of editing something
   * @param $id
   * @return View
   */
  public function getEdit( $id )
  {
    $allPages = $this->pages->getAll();

    try{
      $item = $this->model->requireById($id);
    } catch( EntityNotFoundException $e ){
      return Redirect::to( $this->object_url )->with('errors', new MessageBag( array("An item with the ID:$id could not be found.") ) );
    }

    if( !View::exists( 'kainav::'.$this->view_key.'.edit' ) )
      return App::abort(404, 'Page not found');

    return View::make('kainav::'.$this->view_key.'.edit')
      ->with('pages', $allPages)
      ->with( 'item' , $item );
  }

    /**
     * The method to handle the posted data
     * @param  integer $id The ID of the object
     * @return Redirect
     */
    public function postEdit( $id )
    {
        $record = $this->model->requireById( $id );
        $record->fill( Input::all() );

        if( !$record->isValid() )
            return Redirect::to( $this->edit_url.$id )->with( 'errors' , $record->getErrors() );

        // Run the hydration method that populates anything else that is required / runs any other
        // model interactions and save it.
        $record->hydrateRelations()->save();

        // Redirect that shit man! You did good! Validated and saved, man mum would be proud!
        return Redirect::to( $this->object_url )->with( 'success' , new MessageBag( array( 'Item Saved' ) ) );
    }

}
