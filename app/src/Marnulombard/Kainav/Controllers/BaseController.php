<?php
namespace Marnulombard\Kainav\Controllers;
use Illuminate\Routing\Controller;
use View, Config;

abstract class BaseController extends Controller{

    protected $whitelist = array('send');

    /**
     * The URL segment that can be used to access the system
     * @var string
     */
    protected $urlSegment;

    /**
     * __construct
     *
     * @access   public
     * @return \Marnulombard\Kainav\Controllers\BaseController
     */
    public function __construct()
    {
        // Achieve that segment
        $this->urlSegment = Config::get('kainav::app.access_url');

        // Setup composed views and the variables that they require
        $this->beforeFilter( 'adminFilter' , array('except' => $this->whitelist) );
        $composed_views = array( 'kainav::*' );
        View::composer($composed_views, 'Marnulombard\Kainav\Composers\Page');
    }

}
