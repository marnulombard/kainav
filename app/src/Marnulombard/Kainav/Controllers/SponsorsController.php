<?php namespace Marnulombard\Kainav\Controllers;

use Marnulombard\Kainav\Sponsors\SponsorsInterface;
use Illuminate\Support\MessageBag;
use Input, Redirect, Str, View;


class SponsorsController extends ObjectBaseController {

    /**
     * The place to find the views / URL keys for this controller
     * @var string
     */
    protected $view_key = 'sponsors';

  /**
     * Construct Shit
     */
    public function __construct(SponsorsInterface $sponsorsInterface)
    {
      $this->model = $sponsorsInterface;
      parent::__construct();
    }

}
