<?php namespace Marnulombard\Kainav\Controllers;
use Marnulombard\Kainav\Pages\PagesInterface;

class PagesController extends ObjectBaseController {

    /**
     * The place to find the views / URL keys for this controller
     * @var string
     */
    protected $view_key = 'pages';

    /**
     * Construct Shit
     */
    public function __construct( PagesInterface $pagesInterface)
    {
        $this->model = $pagesInterface;
        parent::__construct();
    }

}
