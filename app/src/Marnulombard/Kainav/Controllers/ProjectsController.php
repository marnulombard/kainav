<?php

namespace Marnulombard\Kainav\Controllers;
use Marnulombard\Kainav\Projects\ProjectsInterface;

class ProjectsController extends ObjectBaseController {

    /**
     * The place to find the views / URL keys for this controller
     * @var string
     */
    protected $view_key = 'projects';

    /**
     * Construct Shit
     */
    public function __construct( ProjectsInterface $projects)
    {
        $this->model = $projects;
        parent::__construct();
    }

}
