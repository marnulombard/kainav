<?php namespace Marnulombard\Kainav;

/**
 * ======================================================================================
 * PLEASE NOTE::
 * This package may have been renamed, but it is a clone of Davzie\LaravelBootstrap
 * All credit to the original Author; Not MarnuLombard
 * ======================================================================================
 */


use Illuminate\Support\ServiceProvider;
use Config, Lang, View;

class KainavServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('marnulombard/kainav');

    require __DIR__ . '/filters.php';
    require __DIR__ . '/routes.php';
    require __DIR__ . '/bindings.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
    $loader = \Illuminate\Foundation\AliasLoader::getInstance();
    $loader->alias('User', 'Marnulombard\Kainav\Accounts\User');
    $loader->alias('\BaseController', 'Marnulombard\Kainav\Controllers\BaseController');

    View::addNamespace('kainav', app_path().'/views/Kainav/');
//    Lang::addNamespace('kainav', app_path().'/lang/Kainav/');
    Config::addNamespace('kainav', app_path().'/config/Kainav/');

  }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('kainav');
	}

}
