<?php namespace Marnulombard\Kainav\Frontend\Controllers;

use Marnulombard\Kainav\News\NewsInterface;
use Marnulombard\Kainav\Pages\PagesInterface;
use Marnulombard\Kainav\Projects\ProjectsInterface;
use Marnulombard\Kainav\Settings\SettingsInterface;
use Marnulombard\Kainav\Sliders\SlidersInterface;
use Marnulombard\Kainav\Sponsors\SponsorsInterface;
use Marnulombard\Kainav\TeamMembers\TeamMembersInterface;
use Marnulombard\Kainav\Frontend\Controllers\BaseController;
use App, View;

class FrontEndPagesController extends BaseController
{
  protected $news;
  protected $pages;
  protected $projects;
  protected $teamMembers;
  protected $settings;
  protected $sliders;
  protected $sponsors;

  public function __construct(
    NewsInterface $newsInterface,
    PagesInterface $pagesInterface,
    ProjectsInterface $projectsInterface,
    TeamMembersInterface $teamMembersInterface,
    SettingsInterface $settingsInterface,
    SlidersInterface $sliders,
    SponsorsInterface $sponsorsInterface
  )
  {
    $this->news = $newsInterface;
    $this->pages = $pagesInterface;
    $this->projects = $projectsInterface;
    $this->teamMembers = $teamMembersInterface;
    $this->settings = $settingsInterface;
    $this->sliders = $sliders;
    $this->sponsors = $sponsorsInterface;
  }


  /**
   * Render the home page View
   */
  public function getHome()
  {

    $news = $this->news->getHomePageItems(['internal', 'external', 'youtube']);
    $sliders = $this->sliders->getAll();
    $sponsors = $this->sponsors->getOnlyXOfType( 4 ,'sponsor');

    return View::make('home')
      ->with('news', $news)
      ->with('sliders', $sliders)
      ->with('sponsors', $sponsors);
  }

  /**
   * Render the Team page
   */
  public function getTheTeam()
  {
    $page = $this->getPageContent('the-team');
    $teamMembers = $this->teamMembers->getAll();

    return View::make('the-team')
      ->with('page', $page)
      ->with('teamMembers', $teamMembers);
  }

  /**
   * Render the Individual Research page
   */
  public function getIndividualResearch()
  {
    $page = $this->getPageContent('research-projects');
    $projects = $this->projects->getAllPersonal();

    return View::make('research')
      ->with('page', $page)
      ->with('projects', $projects);
  }

  /**
   * Render any view project view
   */
  public function getResearchProject($projectSlug)
  {
    $project = $this->projects->getByKey($projectSlug);

    if (!$project) {
      App::abort(404);
    }

    return View::make('research-project')
      ->with('page', $project);
  }

  /**
   * Render the Research Programmes page
   */
  public function getResearchProgrammes()
  {
    $page = $this->getPageContent('programmes');
    $projects = $this->projects->getAllProgrammes();

    return View::make('research')
      ->with('page', $page)
      ->with('projects', $projects);
  }


  /**
   * Render the Our Partners page
   */
  public function getOurPartners()
  {
    $page = $this->getPageContent('our-partners');
    $partners = $this->sponsors->getByType('partner');
    $sponsors = $this->sponsors->getByType('sponsor');

    return View::make('our-partners')
      ->with('page', $page)
      ->with('partners', $partners)
      ->with('sponsors', $sponsors);
  }

  /**
   * Render the Contact Page
   * We separate this because the page needs to include the contact form
   */
  public function getContact()
  {
    $page = $this->getPageContent('contact-us');
    $contact_number = $this->settings->getByKey('contact_number')->value;
    $facebook_link = $this->settings->getByKey('facebook-link')->value;

    return View::make('contact-us')
      ->with('page', $page)
      ->with('contact_number', $contact_number)
      ->with('facebook_link', $facebook_link);
  }


  /**
   * Render any view without a view
   */
  public function getPage($slug)
  {
    $page = $this->getPageContent($slug);

    if (!$page) {
      App::abort(404);
    }

    return View::make('generic')
      ->with('page', $page);
  }

  /**
   * Get a page’s content by the slug (url)
   */
  public function getPageContent($slug)
  {
    return $this->pages->getBySlug($slug);
  }
}
