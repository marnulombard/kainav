<?php  namespace Marnulombard\Kainav\Frontend\ViewComposers;

use Marnulombard\Kainav\Pages\Pages;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Collection;
use Illuminate\View\View;

class NavigationViewComposer
{
  public function compose(View $view)
  {
    $nav = new Collection;

    $links = Pages::whereRaw("`type` = 'link' OR `type` = 'dropDown'")->get();
    $subLinks = Pages::where('type', '=', 'subLink')->get();

    foreach ($links as $link) {

      $dropDownLinks = new Collection;

      foreach($subLinks as $subLink)
      {
        if($subLink['linked_to'] === $link['slug'])
        {
          $dropDownLinks->push((object)[
            'title' => $subLink['title'],
            'type' => $subLink['type'],
            'slug'  => URL::to($subLink['slug']),
          ]);
        }
      }
      $nav->push((object)[
        'title' => $link['title'],
        'type' => $link['type'],
        'slug'  => $link['slug'],
        'subLinks' => $dropDownLinks,
      ]);
    }

    /*
    |------------------------------------------------------------------------
    | Save the menu as data for the view
    |------------------------------------------------------------------------
    | Any properties set on the $view variable will be available in our view
    | under that key.
    */
    $view->nav = $nav;
    $view->subNav = $subLinks;

  }
} 
