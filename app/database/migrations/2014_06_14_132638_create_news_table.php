<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if ( !Schema::hasTable('news') )
    {
      Schema::create('news', function(Blueprint $table)
      {
        $table->engine = 'InnoDB';

        $table->increments('id');
        $table->index('id');

        $table->string('title');
        $table->text('content');
        $table->timestamps();
      });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('news');
  }

}
