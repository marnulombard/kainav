<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('sliders') )
        {
            Schema::create('sliders', function($table)
            {
                $table->engine = 'InnoDB';

                $table->increments('id');
                $table->index('id');

                $table->string('title');
                $table->string('content');
                $table->string('on_page');
                $table->string('order')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sliders');
    }

 }
