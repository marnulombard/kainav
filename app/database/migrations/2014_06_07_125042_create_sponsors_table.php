<?php

use Illuminate\Database\Migrations\Migration;

class CreateSponsorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    if ( !Schema::hasTable('sponsors') )
    {
      Schema::create('sponsors', function($table)
      {
        $table->engine = 'InnoDB';

        $table->increments('id');
        $table->string('title');
        $table->text('content');
        $table->string('url');
        $table->unique('url');
        $table->index('id');
        $table->timestamps();
      });
    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sponsors');
	}

}
