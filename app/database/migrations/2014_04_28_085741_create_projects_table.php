<?php
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('projects') )
        {
            Schema::create('projects', function($table)
            {

              $table->engine = 'InnoDB';

              $table->increments('id');
              $table->string('title',255);
              $table->string('key',255);
              $table->unique('key');
              $table->string('year');
              $table->string('category');
              $table->string('caption');
              $table->text('content');
              $table->index('id');
              $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }

}
