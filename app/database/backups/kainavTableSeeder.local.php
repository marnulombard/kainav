<?php

class kainavTableSeeder extends Seeder {
    public function run()
    {
        DB::table('blocks')->insert(
                    
            array(
                'id' => 1,
                'title' => '',
                'key' => '',
                'content' => '',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            )

        );DB::table('blog')->insert(
                    
            array(
                'id' => 1,
                'title' => '',
                'key' => '',
                'content' => '',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            )

        );DB::table('galleries')->insert(
                    
            array(
                'id' => 1,
                'title' => '',
                'slug' => '',
                'description' => '',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            )

        );
        DB::table('news')->insert(array(
                    
            array(
                'id' => 1,
                'title' => 'A fake internal news item',
                'content' => '<p>
	  qwrtwredhjfdhdghdsrtyertyedrty
</p>',
                'created_at' => '2014-06-24 19:11:24',
                'updated_at' => '2014-06-24 19:12:38',
                'type' => 'internal',
            ),

            array(
                'id' => 2,
                'title' => 'A news Item from outside Kainav',
                'content' => '<p>
	 fyjgfhjghjghjkgkjghjkghjghjkghjk
</p>',
                'created_at' => '2014-06-24 19:13:39',
                'updated_at' => '2014-06-24 19:14:31',
                'type' => 'external',
            ),

            array(
                'id' => 3,
                'title' => 'A youtube Vid',
                'content' => '<iframe width="420" height="315" src="//www.youtube.com/embed/u5CVsCnxyXg" frameborder="0" allowfullscreen="">
</iframe>',
                'created_at' => '2014-06-24 19:16:22',
                'updated_at' => '2014-06-24 19:16:22',
                'type' => 'youtube',
            ),

        ));
        DB::table('pages')->insert(array(
                    
            array(
                'id' => 2,
                'title' => 'Home',
                'slug' => 'home',
                'type' => 'link',
                'linked_to' => '',
                'content' => '',
                'created_at' => '2014-06-08 15:45:59',
                'updated_at' => '2014-06-08 15:45:59',
                'launched' => 1,
            ),

            array(
                'id' => 3,
                'title' => 'About Us',
                'slug' => 'about-us',
                'type' => 'dropDown',
                'linked_to' => '',
                'content' => '',
                'created_at' => '2014-06-08 15:46:12',
                'updated_at' => '2014-06-08 15:46:12',
                'launched' => 1,
            ),

            array(
                'id' => 4,
                'title' => 'The Foundation',
                'slug' => 'the-foundation',
                'type' => 'subLink',
                'linked_to' => 'about-us',
                'content' => '<p>
	  Founded in 2011, by twin brothers Kailen and Navelan Padayachee, The KaiNav Conservation Foundation was established with a unique outlook on biodiversity conservation and research. The foundation believes in including people from all walks of life in all facets of biodiversity conservation to emphasize the benefits of healthy ecosystems in every day life for all humanity.
</p>
<p>
	  The KaiNav Conservation Foundation is a registered not-for-profit environmental research organization, (Incorporated Under Section 21) Registration number: 2011/000496/08.
</p>
<p>
	  KaiNav Conservation Foundation seeks to aid in biodiversity conservation through sound scientific research and education that identifies and promotes sustainable utilization of natural resources.
</p>
<p>
	  The foundation, its directors and employees are driven to promote and encourage young environmental scientists to address and solve environmental issues using new and innovative techniques and ideas.
</p>',
                'created_at' => '2014-06-08 15:47:31',
                'updated_at' => '2014-06-17 19:39:58',
                'launched' => 1,
            ),

            array(
                'id' => 5,
                'title' => 'The Team',
                'slug' => 'the-team',
                'type' => 'subLink',
                'linked_to' => 'about-us',
                'content' => '',
                'created_at' => '2014-06-08 15:47:49',
                'updated_at' => '2014-06-08 15:47:49',
                'launched' => 1,
            ),

            array(
                'id' => 6,
                'title' => 'Research',
                'slug' => 'research',
                'type' => 'dropDown',
                'linked_to' => '',
                'content' => '',
                'created_at' => '2014-06-08 16:09:44',
                'updated_at' => '2014-06-08 16:09:44',
                'launched' => 1,
            ),

            array(
                'id' => 7,
                'title' => 'Research Projects',
                'slug' => 'research-projects',
                'type' => 'subLink',
                'linked_to' => 'research',
                'content' => '',
                'created_at' => '2014-06-08 16:10:58',
                'updated_at' => '2014-06-17 20:19:18',
                'launched' => 1,
            ),

            array(
                'id' => 8,
                'title' => 'Programmes',
                'slug' => 'programmes',
                'type' => 'subLink',
                'linked_to' => 'research',
                'content' => '',
                'created_at' => '2014-06-08 16:11:46',
                'updated_at' => '2014-06-17 06:26:02',
                'launched' => 0,
            ),

            array(
                'id' => 9,
                'title' => 'Get Involved',
                'slug' => 'get-involved',
                'type' => 'dropDown',
                'linked_to' => '',
                'content' => '',
                'created_at' => '2014-06-08 16:12:07',
                'updated_at' => '2014-06-08 16:12:07',
                'launched' => 1,
            ),

            array(
                'id' => 10,
                'title' => 'Donate',
                'slug' => 'donate',
                'type' => 'subLink',
                'linked_to' => 'get-involved',
                'content' => '',
                'created_at' => '2014-06-08 16:12:23',
                'updated_at' => '2014-06-17 06:26:16',
                'launched' => 0,
            ),

            array(
                'id' => 11,
                'title' => 'Volunteer',
                'slug' => 'volunteer',
                'type' => 'subLink',
                'linked_to' => 'get-involved',
                'content' => '',
                'created_at' => '2014-06-08 16:12:38',
                'updated_at' => '2014-06-17 06:37:16',
                'launched' => 0,
            ),

            array(
                'id' => 12,
                'title' => 'Our Partners',
                'slug' => 'our-partners',
                'type' => 'link',
                'linked_to' => '',
                'content' => '',
                'created_at' => '2014-06-08 16:13:04',
                'updated_at' => '2014-06-08 16:13:04',
                'launched' => 1,
            ),

            array(
                'id' => 13,
                'title' => 'Contact Us',
                'slug' => 'contact-us',
                'type' => 'link',
                'linked_to' => '',
                'content' => '',
                'created_at' => '2014-06-08 16:13:14',
                'updated_at' => '2014-06-08 16:13:14',
                'launched' => 1,
            ),

        ));
        DB::table('projects')->insert(array(
                    
            array(
                'id' => 1,
                'title' => 'Harbour Predator',
                'key' => 'harbour-predator',
                'year' => 2013,
                'category' => 'personal',
                'caption' => 'Short description of the project',
                'content' => '<p>
	The Harbour Predator Research Project analyses and monitors the status of the predatory fauna occurring in the highly developed harbours of South Africa. This project uses predators as biological indicators of ecosystem health.
</p>
<p>
	<strong>Diurnal predatory fish species assemblage and diversity: The use of BRUVS in the Durban harbor, South Africa</strong>
</p>
<p>
	This study is attempting to determine the fish species diversity and assemblage of Durban’s commercial port.  The study is using Baited Remote Underwater Video Systems (BRUVS) to record the different fish species found in the different habitats within the harbor. The study focuses on both the natural habitats such as mangrove ecosystems and shallow sand banks as well as man made habitats, including boat docks, piers, jetties etc. The results of each habitat will be compared in order to determine if there is a difference between the fish species diversity and assemblage of natural habitats and man made habitats.
</p>',
                'created_at' => '2014-06-09 05:28:58',
                'updated_at' => '2014-06-12 20:28:22',
            ),

            array(
                'id' => 2,
                'title' => 'Urban Raptor Research Project',
                'key' => 'urban-raptor-research-project',
                'year' => 2013,
                'category' => 'personal',
                'caption' => 'Short description of the project',
                'content' => '<p>
	The Urban Raptor Research Project analyses and monitors birds of prey, which have adapted to survive in and around urban centers. These species are at the top of their respective food chains and are regarded as apex predators and are therefore used as biological indicators of the ecosystems health and stability.
</p>
<h4>Wonderboom Urban Verreaux’s eagle research</h4>
<p>
	This research was conducted between 2011 and 2012, the diet of a pair of Verreaux’s eagles nesting in the densely urbanized center of the city of Pretoria, South Africa, was studied in order to assess the impact high levels of urban development have on the diet composition and therefore prey selection of urban Verreaux’s eagles.
</p>
<h4>Magaliesburg Verreaux’s eagle research</h4>
<p>
	This research is currently underway and assesses the diet composition and prey selection of four pairs of Verreaux’s eagles nesting along the Magaliesburg mountain range crossing the Gauteng and North West Provinces, South Africa, using camera traps to capture instant images of prey provisions to young. In addition to diet composition, the nest site selection of these raptors is also being investigated and analyzed in order to determine the effects of various land uses and urban development on both diet and nest site selection of Verreaux’s eagles.
</p>',
                'created_at' => '2014-06-09 05:28:58',
                'updated_at' => '2014-06-12 20:30:03',
            ),

            array(
                'id' => 4,
                'title' => 'Urban Jungle Program',
                'key' => 'urban-jungle-program',
                'year' => 2013,
                'category' => 'programme',
                'caption' => 'As urban areas continue to grow and expand, particularly in developing countries such as South Africa, the frequency with which humans come into contact with the natural world, increases.',
                'content' => '<p>
	   As urban areas continue to grow and expand, particularly in developing countries such as South Africa, the frequency with which humans come into contact with the natural world, increases.
</p>
<p>
	   As these urban centers expand, development associated with human activities often encroach into previously natural areas, forcing wildlife to either adapt to a new and unique environment or abandon their natural homes in search of more suitable habitats.
</p>
<p>
	   This urban onslaught on the natural world has prompted a relatively new form of nature conservation in order to:
</p>
<ol>
	<li>Understand the effects of urbanization on wildlife ecology and find ways for humans and wildlife to survive together in harmony. And</li>
	<li>To develop methods of conserving and protecting natural environments within urban centers in order to enjoy the benefits of healthy and functioning natural ecosystems.</li>
</ol>
<p>
	   The KaiNav Conservation Foundation’s Urban Jungle Program include different research projects which help us gain a better understanding of environmental health and function in and around urban centers.
</p>',
                'created_at' => '2014-06-12 20:26:44',
                'updated_at' => '2014-06-12 20:26:44',
            ),

        ));
        DB::table('settings')->insert(array(
                    
            array(
                'id' => 1,
                'key' => 'application_name',
                'label' => 'Application Name',
                'value' => 'Kainav Conservation Foundation',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),

            array(
                'id' => 2,
                'key' => 'contact_email',
                'label' => 'Contact Email',
                'value' => 'admin@kainavconservation.co.za',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),

            array(
                'id' => 3,
                'key' => 'facebook-link',
                'label' => 'Facebook Link',
                'value' => 'https://www.facebook.com/pages/Kainav-Conservation-Foundation/1427336384169525',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),

            array(
                'id' => 4,
                'key' => 'contact_number',
                'label' => 'Contact Number',
                'value' => '+27 11 467 6580',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '2014-06-17 20:17:54',
            ),

        ));
        DB::table('sliders')->insert(array(
                    
            array(
                'id' => 2,
                'title' => 'Dolphin',
                'content' => '<p>
	“Like music and art, love of nature is a common language that can transcend political or social boundaries.”
</p>
<p>
	<em>- Jimmy Carter</em>
</p>',
                'on_page' => 'home',
                'order' => '',
                'created_at' => '2014-06-09 05:07:59',
                'updated_at' => '2014-06-17 19:58:37',
            ),

            array(
                'id' => 3,
                'title' => 'Tutrtle',
                'content' => '<p>
	 KaiNav Conservation Foundation is dedicated to Africa’s biodiversity and cultural heritage as well as the sustainable development of rural communities.
</p>',
                'on_page' => 'home',
                'order' => '',
                'created_at' => '2014-06-09 05:08:37',
                'updated_at' => '2014-06-09 05:08:47',
            ),

            array(
                'id' => 4,
                'title' => 'Hormbill',
                'content' => '<p>
	   "We do not inherit the earth from our ancestors, we borrow it from our children."
</p>
<p>
	 <em>- Native American Proverb</em>
</p>',
                'on_page' => 'home',
                'order' => '',
                'created_at' => '2014-06-17 20:06:53',
                'updated_at' => '2014-06-24 20:03:49',
            ),

        ));
        DB::table('sponsors')->insert(array(
                    
            array(
                'id' => 5,
                'title' => 'Spiral Engineering',
                'content' => '<p>
	A service they provided
</p>',
                'url' => 'http://spiralengineering.co.za',
                'created_at' => '2014-06-16 11:11:33',
                'updated_at' => '2014-06-16 12:13:00',
                'type' => 'sponsor',
            ),

            array(
                'id' => 6,
                'title' => 'Kingfisher',
                'content' => '<p>
	A service they provided
</p>',
                'url' => 'http://kingfisher.co.za',
                'created_at' => '2014-06-16 11:11:33',
                'updated_at' => '2014-06-16 12:13:38',
                'type' => 'sponsor',
            ),

            array(
                'id' => 7,
                'title' => 'Marnu Lombard',
                'content' => '<p>
	Logo design, design and development of the website
</p>',
                'url' => 'http://marnulombard.com',
                'created_at' => '2014-06-16 11:11:33',
                'updated_at' => '2014-06-16 12:13:19',
                'type' => 'sponsor',
            ),

            array(
                'id' => 8,
                'title' => 'PM Africa',
                'content' => '<p>
	A service they provided
</p>',
                'url' => 'http://pmafrica.com',
                'created_at' => '2014-06-16 11:11:33',
                'updated_at' => '2014-06-16 12:12:46',
                'type' => 'sponsor',
            ),

        ));
        DB::table('tags')->insert(array(
                    
            array(
                'id' => 4,
                'tag' => '',
                'taggable_id' => 3,
                'taggable_type' => 'Marnulombard\Kainav\TeamMembers\TeamMembers',
                'created_at' => '2014-06-08 14:36:13',
                'updated_at' => '2014-06-08 14:36:13',
            ),

            array(
                'id' => 5,
                'tag' => '',
                'taggable_id' => 1,
                'taggable_type' => 'Marnulombard\Kainav\Pages\Pages',
                'created_at' => '2014-06-08 15:45:17',
                'updated_at' => '2014-06-08 15:45:17',
            ),

            array(
                'id' => 7,
                'tag' => '',
                'taggable_id' => 3,
                'taggable_type' => 'Marnulombard\Kainav\Sliders\Sliders',
                'created_at' => '2014-06-09 05:08:47',
                'updated_at' => '2014-06-09 05:08:47',
            ),

            array(
                'id' => 9,
                'tag' => '',
                'taggable_id' => 3,
                'taggable_type' => 'Marnulombard\Kainav\Sponsors\Sponsors',
                'created_at' => '2014-06-09 17:40:35',
                'updated_at' => '2014-06-09 17:40:35',
            ),

            array(
                'id' => 17,
                'tag' => '',
                'taggable_id' => 8,
                'taggable_type' => 'Marnulombard\Kainav\Pages\Pages',
                'created_at' => '2014-06-17 06:26:02',
                'updated_at' => '2014-06-17 06:26:02',
            ),

            array(
                'id' => 19,
                'tag' => '',
                'taggable_id' => 10,
                'taggable_type' => 'Marnulombard\Kainav\Pages\Pages',
                'created_at' => '2014-06-17 06:26:31',
                'updated_at' => '2014-06-17 06:26:31',
            ),

            array(
                'id' => 20,
                'tag' => '',
                'taggable_id' => 11,
                'taggable_type' => 'Marnulombard\Kainav\Pages\Pages',
                'created_at' => '2014-06-17 06:37:16',
                'updated_at' => '2014-06-17 06:37:16',
            ),

            array(
                'id' => 31,
                'tag' => '',
                'taggable_id' => 4,
                'taggable_type' => 'Marnulombard\Kainav\Pages\Pages',
                'created_at' => '2014-06-17 19:39:58',
                'updated_at' => '2014-06-17 19:39:58',
            ),

            array(
                'id' => 33,
                'tag' => '',
                'taggable_id' => 2,
                'taggable_type' => 'Marnulombard\Kainav\TeamMembers\TeamMembers',
                'created_at' => '2014-06-17 19:43:57',
                'updated_at' => '2014-06-17 19:43:57',
            ),

            array(
                'id' => 35,
                'tag' => '',
                'taggable_id' => 1,
                'taggable_type' => 'Marnulombard\Kainav\TeamMembers\TeamMembers',
                'created_at' => '2014-06-17 19:49:16',
                'updated_at' => '2014-06-17 19:49:16',
            ),

            array(
                'id' => 39,
                'tag' => '',
                'taggable_id' => 2,
                'taggable_type' => 'Marnulombard\Kainav\Sliders\Sliders',
                'created_at' => '2014-06-17 19:58:37',
                'updated_at' => '2014-06-17 19:58:37',
            ),

            array(
                'id' => 40,
                'tag' => '',
                'taggable_id' => 1,
                'taggable_type' => 'Marnulombard\Kainav\Sliders\Sliders',
                'created_at' => '2014-06-17 20:07:47',
                'updated_at' => '2014-06-17 20:07:47',
            ),

            array(
                'id' => 41,
                'tag' => '',
                'taggable_id' => 7,
                'taggable_type' => 'Marnulombard\Kainav\Pages\Pages',
                'created_at' => '2014-06-17 20:19:18',
                'updated_at' => '2014-06-17 20:19:18',
            ),

            array(
                'id' => 44,
                'tag' => '',
                'taggable_id' => 4,
                'taggable_type' => 'Marnulombard\Kainav\Sliders\Sliders',
                'created_at' => '2014-06-24 20:03:49',
                'updated_at' => '2014-06-24 20:03:49',
            ),

        ));
        DB::table('team_members')->insert(array(
                    
            array(
                'id' => 1,
                'name' => 'Kailen Padayachee',
                'title' => 'Executive Director | Nature Conservationist',
                'content' => '<p>
	   Kailen is an ecologist from Johannesburg South Africa with experience in environmental education, marine fish husbandry and capture and raptor ecology. He has a passion for biodiversity conservation and research and believes that the innovative and sustainable utilization of natural resources is the key to conserving the world’s wildlife and ecosystems efficiently and effectively. Kailen uses his additional skills gained as a SCUBA diver, pilot and volunteer search and rescue technician to complete his research in the field.
</p>',
                'created_at' => '2014-06-07 14:08:00',
                'updated_at' => '2014-06-17 19:49:16',
            ),

            array(
                'id' => 2,
                'name' => 'Navelan Padayachee',
                'title' => 'Executive Director | Nature Conservationist',
                'content' => '<p>
	 Navelan is an ecologist currently studying fish species in estuarine and mangrove ecosystems. His past experience in environmental education, marine fish husbandry and capture and field research in different marine environments has fueled his passion for biodiversity conservation and educating the public about the natural environment and the issues surrounding it. Navelan’s additional skills include being rescue diver qualified and a volunteer search and rescue technician.
</p>',
                'created_at' => '2014-06-07 14:08:00',
                'updated_at' => '2014-06-17 19:43:57',
            ),

            array(
                'id' => 3,
                'name' => 'Mogani Padayachee',
                'title' => 'Managing Member',
                'content' => '<p>
	Architect and entrepreneur Mogani has embarked on a creative career after a successfully qualifying as an architect. She worked in all aspects of the architectural arena including the interior designing ambit. She was involved in commercial, industrial, high cost as well as low cost housing. She has since after a number of years as a design architect, entered the business world and currently is a successful entrepreneur with various business interests to her name, amongst which is the successful Dreamnails Franchise and a Steers Franchise. Mogani has, since the return of her sons, Kailen and Navelan from the USA, facilitated and supported their initiatives in the establishment and management of KCF. Her business track record and acumen reinforced her input into the provisions of managerial skills and advice to both Kailen and Navelan in terms of the stated objectives of KCF, a Section 21 Company.
</p>',
                'created_at' => '2014-06-07 14:08:00',
                'updated_at' => '2014-06-08 14:36:13',
            ),

        ));
        DB::table('uploads')->insert(array(
                    
            array(
                'id' => 1,
                'uploadable_type' => 'Marnulombard\Kainav\TeamMembers\TeamMembers',
                'uploadable_id' => 1,
                'path' => 'team_members',
                'filename' => '9c0f90ed5e640d81eaa198eb69ec2f7acdf829fe.png',
                'extension' => 'png',
                'order' => 0,
                'created_at' => '2014-06-08 14:33:34',
                'updated_at' => '2014-06-15 10:35:23',
            ),

            array(
                'id' => 3,
                'uploadable_type' => 'Marnulombard\Kainav\TeamMembers\TeamMembers',
                'uploadable_id' => 2,
                'path' => 'team_members',
                'filename' => '468c75504ce2819b522404f2e6feaada5a6bdb91.png',
                'extension' => 'png',
                'order' => 999,
                'created_at' => '2014-06-08 14:36:01',
                'updated_at' => '2014-06-08 14:36:01',
            ),

            array(
                'id' => 4,
                'uploadable_type' => 'Marnulombard\Kainav\TeamMembers\TeamMembers',
                'uploadable_id' => 3,
                'path' => 'team_members',
                'filename' => '5342c8c00d030db10fcc9e42f58a1fb6807bfc4f.png',
                'extension' => 'png',
                'order' => 999,
                'created_at' => '2014-06-08 14:36:13',
                'updated_at' => '2014-06-08 14:36:13',
            ),

            array(
                'id' => 6,
                'uploadable_type' => 'Marnulombard\Kainav\Sliders\Sliders',
                'uploadable_id' => 3,
                'path' => 'sliders',
                'filename' => '7e0ea2132db5377b7930d4b973cf8294c1a17afb.jpg',
                'extension' => 'jpg',
                'order' => 999,
                'created_at' => '2014-06-09 05:08:47',
                'updated_at' => '2014-06-09 05:08:47',
            ),

            array(
                'id' => 7,
                'uploadable_type' => 'Marnulombard\Kainav\Sliders\Sliders',
                'uploadable_id' => 2,
                'path' => 'sliders',
                'filename' => '8f01589af83d9179a8c7e34bc5b31a92edf69da9.jpg',
                'extension' => 'jpg',
                'order' => 999,
                'created_at' => '2014-06-09 05:08:54',
                'updated_at' => '2014-06-09 05:08:54',
            ),

            array(
                'id' => 8,
                'uploadable_type' => 'Marnulombard\Kainav\Sponsors\Sponsors',
                'uploadable_id' => 3,
                'path' => 'sponsors',
                'filename' => 'ea2f0f15513e4aec89c848c7b34d4313e2532fd8.png',
                'extension' => 'png',
                'order' => 999,
                'created_at' => '2014-06-09 17:40:35',
                'updated_at' => '2014-06-09 17:40:35',
            ),

            array(
                'id' => 9,
                'uploadable_type' => 'Marnulombard\Kainav\Sponsors\Sponsors',
                'uploadable_id' => 4,
                'path' => 'sponsors',
                'filename' => '9e1598793f51f9a9334865735f1b0fc374b58b05.png',
                'extension' => 'png',
                'order' => 999,
                'created_at' => '2014-06-09 17:42:09',
                'updated_at' => '2014-06-09 17:42:09',
            ),

            array(
                'id' => 10,
                'uploadable_type' => 'Marnulombard\Kainav\Sponsors\Sponsors',
                'uploadable_id' => 1,
                'path' => 'sponsors',
                'filename' => '0ee5dd758bdcf05eba098a51b401b44ca464a949.png',
                'extension' => 'png',
                'order' => 999,
                'created_at' => '2014-06-09 17:42:44',
                'updated_at' => '2014-06-09 17:42:44',
            ),

            array(
                'id' => 11,
                'uploadable_type' => 'Marnulombard\Kainav\Sponsors\Sponsors',
                'uploadable_id' => 2,
                'path' => 'sponsors',
                'filename' => 'af4a76ba292bfda4615cc2d3ba99eac614f44692.png',
                'extension' => 'png',
                'order' => 999,
                'created_at' => '2014-06-09 17:43:05',
                'updated_at' => '2014-06-09 17:43:05',
            ),

            array(
                'id' => 12,
                'uploadable_type' => 'Marnulombard\Kainav\Sponsors\Sponsors',
                'uploadable_id' => 8,
                'path' => 'sponsors',
                'filename' => '6145dd07749c0be92233a5dd01d3566ab272701a.png',
                'extension' => 'png',
                'order' => 999,
                'created_at' => '2014-06-16 12:12:46',
                'updated_at' => '2014-06-16 12:12:46',
            ),

            array(
                'id' => 13,
                'uploadable_type' => 'Marnulombard\Kainav\Sponsors\Sponsors',
                'uploadable_id' => 5,
                'path' => 'sponsors',
                'filename' => '479413837da748f678d4964f70aaff6c38b9217e.png',
                'extension' => 'png',
                'order' => 999,
                'created_at' => '2014-06-16 12:13:00',
                'updated_at' => '2014-06-16 12:13:00',
            ),

            array(
                'id' => 14,
                'uploadable_type' => 'Marnulombard\Kainav\Sponsors\Sponsors',
                'uploadable_id' => 7,
                'path' => 'sponsors',
                'filename' => 'f893f8b2dd1d3520d3d3d10618eadc7d94325c6e.png',
                'extension' => 'png',
                'order' => 999,
                'created_at' => '2014-06-16 12:13:19',
                'updated_at' => '2014-06-16 12:13:19',
            ),

            array(
                'id' => 15,
                'uploadable_type' => 'Marnulombard\Kainav\Sponsors\Sponsors',
                'uploadable_id' => 6,
                'path' => 'sponsors',
                'filename' => '2ba3785ccef43de2c3998da454ffe591ea9db584.png',
                'extension' => 'png',
                'order' => 999,
                'created_at' => '2014-06-16 12:13:38',
                'updated_at' => '2014-06-16 12:13:38',
            ),

            array(
                'id' => 17,
                'uploadable_type' => 'Marnulombard\Kainav\News\News',
                'uploadable_id' => 1,
                'path' => 'news',
                'filename' => '11a8244dc78825b4d7b604e547c5bcd8df2615ab.JPG',
                'extension' => 'JPG',
                'order' => 999,
                'created_at' => '2014-06-24 19:12:38',
                'updated_at' => '2014-06-24 19:12:38',
            ),

            array(
                'id' => 18,
                'uploadable_type' => 'Marnulombard\Kainav\News\News',
                'uploadable_id' => 2,
                'path' => 'news',
                'filename' => '85cea85bbd32bbfd81971393e9983efcbe85866f.JPG',
                'extension' => 'JPG',
                'order' => 999,
                'created_at' => '2014-06-24 19:14:31',
                'updated_at' => '2014-06-24 19:14:31',
            ),

            array(
                'id' => 19,
                'uploadable_type' => 'Marnulombard\Kainav\Sliders\Sliders',
                'uploadable_id' => 4,
                'path' => 'sliders',
                'filename' => 'c72984eb8ef09c7f907f8c84c349026df82559b3.JPG',
                'extension' => 'JPG',
                'order' => 999,
                'created_at' => '2014-06-24 20:03:49',
                'updated_at' => '2014-06-24 20:03:49',
            ),

        ));DB::table('users')->insert(
                    
            array(
                'id' => 1,
                'email' => 'admin@kainavconservation.co.za',
                'password' => '$2y$10$kxPSUJVLX0eB7/rfATcVVOBCtN3mt3jdrm5finu99BOzYL4nvuW8C',
                'first_name' => 'Marnu',
                'last_name' => 'Lombard',
                'remember_token' => 'RK05ZhIZMBntsiAFFsNhnLGORDgB9bIy7ulMf4ABMd0062CE0JzOdhdV4awP',
                'last_login' => '2014-06-24 17:49:46',
                'created_at' => '2014-06-07 17:36:40',
                'updated_at' => '2014-06-24 17:49:46',
            )

        );
    }
}