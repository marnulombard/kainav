<?php
namespace Marnulombard\Kainav\Seeds;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class ExampleSettingsSeeder extends Seeder {

    public function run()
    {

        $types = [
          [
              'key'           => 'application_name',
              'label'         => 'Application Name',
              'value'         => Config::get('kainav::app.name')
          ],
          [
              'key'           => 'contact_email',
              'label'         => 'ContactEmail',
              'value'         => Config::get('kainav::app.support_email')
          ],

        ];
        DB::table('settings')->insert($types);
        $this->command->info('Settings Table Seeded');

    }

}
