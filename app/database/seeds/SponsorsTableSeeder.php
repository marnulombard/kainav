<?php namespace Marnulombard\Kainav\Seeds;

use Illuminate\Database\Seeder;
use Marnulombard\Kainav\Sponsors\Sponsors;
use Schema, DB;

class SponsorsTableSeeder extends Seeder {

public $sponsors = array(
  0 => array(
    'title'      => 'Spiral Engineering',
    'content'   => 'A service they provided',
    'url'       => 'http://spiralengineering.co.za',
    'type' => 'sponsor',
  ),
  1 => array(
    'title'      => 'Kingfisher',
    'content'   => 'A service they provided',
    'url'       => 'http://kingfisher.co.za',
    'type' => 'sponsor',
  ),
  2 => array(
    'title'      => 'Marnu Lombard',
    'content'   => 'Logo design, design and development of the website',
    'url'       => 'http://marnulombard.com',
    'type' => 'sponsor',
  ),
  3 => array(
    'title'      => 'PM Africa',
    'content'   => 'A service they provided',
    'url'       => 'http://pmafrica.com',
    'type' => 'sponsor',
  ),
);


  public function run()
	{
    if(Schema::hasTable('sponsors'))
    {
      DB::table('sponsors')->delete();
    }

    foreach ($this->sponsors as $sponsor) {
      Sponsors::create([
        'title' => $sponsor['title'],
        'content' => $sponsor['content'],
        'url' => $sponsor['url'],
        'type' => $sponsor['type'],
      ]);
    }
	}

}
