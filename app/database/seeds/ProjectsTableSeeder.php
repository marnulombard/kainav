<?php namespace Marnulombard\Kainav\Seeds;

use Illuminate\Database\Seeder;
use Marnulombard\Kainav\Projects\Projects;
use Schema, DB;

class ProjectsTableSeeder extends Seeder {

public $projects = array(
  0 => array(
    'name'      => 'Harbour Predator',
    'year'      => '2013',
    'caption'   => 'Short description of the project',
    'category'  => 'personal',
    'content'   => 'The body of the project goes here',
    'key'       => 'harbour-predator-research-project'
  ),
  1 => array(
    'name'      => 'Verreaux’s Eagle Project',
    'year'      => '2013',
    'caption'   => 'Short description of the project',
    'category'  => 'personal',
    'content'   => 'The body of the project goes here',
    'key'       => 'verreauxs-eagle-project'
  ),
  2 => array(
    'name'      => 'Verreaux’s Eagle Proposal',
    'year'      => '2013',
    'caption'   => 'Short description of the project',
    'category'  => 'personal',
    'content'   => 'The body of the project goes here',
    'key'       => 'verreauxs-eagle-proposal'
  )
);


  public function run()
	{
    if(Schema::hasTable('projects'))
    {
      DB::table('projects')->delete();
    }

    foreach ($this->projects as $project) {
      Projects::create([
        'title' => $project['name'],
        'year' => $project['year'],
        'caption' => $project['caption'],
        'category'  => $project['category'],
        'content' => $project['content'],
        'key' => $project['key'],
      ]);
    }
	}

}
