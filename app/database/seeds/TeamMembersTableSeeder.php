<?php  namespace Marnulombard\Kainav\Seeds;

use Illuminate\Database\Seeder;
use Marnulombard\Kainav\TeamMembers\TeamMembers;
use Schema, DB;

class TeamMembersTableSeeder extends Seeder {

  public $teamMembers = array(
    0 => array(
      'name'      => 'Kailen Padayachee',
      'title'   => 'Executive Member | Nature Conservationist',
      'content'   => '<p>Kailen studied nature conservation at the Tshwane University of Technology in Pretoria, South Africa, as part of his qualification; he took part in a conservation internship with SAAMBR (The South African Association for Marine Biological Research) as a conservationist at the Sea World Aquarium in Ushaka, Durban, South Africa. During his time at sea world Kailen gained experience in environmental education, marine animal husbandry and capture and collection of marine specimens. After qualifying in nature conservation, Kailen was chosen to participate as a cultural representative on a cultural program for the Walt Disney Company in Orlando, USA. Whilst working for Walt Disney, Orlando, Kailen developed a great appreciation and respect for African culture and realized the importance of cultural conservation along with wildlife conservation and it’s interrelationships. Kailen is an avid scuba diver, outdoorsman and an amateur wildlife photographer. With a love for the natural environment and cultural heritage of the African continent, he strives to be the change in the world by promoting the wonders of the African continent through environmental education. Kailen is currently undertaking his masters in nature conservation through the Tshwane University of Technology and hopes to in the next three years obtain his doctorate in nature conservation, which will add to his current platform for the promotion of conservation on the African continent and globally.</p>',
    ),
    1 => array(
      'name'      => 'Navelan Padayachee',
      'title'   => 'Executive Member | Nature Conservationist',
      'content'   => '<p>Navelan studied Nature Conservation at the Tswane University of Technology in Pretoria. As part of his Nature conservation qualification, Navelan took part in an internship with SAAMBR (South African Association for Marine Biological Research) at the uShaka Marine World where his job included environmental education as well as marine animal husbandry. After completing the SAAMBR internship Navelan moved to the United States of America for a year and worked for The Walt Disney Company, Orlando as a cultural representative and environmental and conservation educationist, Navelan returned to South Africa with the goal of finding a way to create an environmental and cultural awareness and tolerance. Navelan is a qualified fitness instructor, avid scuba diver and outdoorsman with a passion for nature, adventure and exploration. He also has a passion for environmental education and teaching others about Africa’s amazing natural environment, beautiful people and cultures. Navelan is currently undertaking his master’s degree in nature conservation through the Tshwane University of Technology and hopes to continue his quest in adding value to the betterment of conservation in Africa and the globe.</p>',
    ),
    3 => array(
      'name'      => 'Mogani Padayachee',
      'title'   => 'Managing Member',
      'content'   => '<p>Architect and entrepreneur Mogani has embarked on a creative career after a successfully qualifying as an architect. She worked in all aspects of the architectural arena including the interior designing ambit. She was involved in commercial, industrial, high cost as well as low cost housing. She has since after a number of years as a design architect, entered the business world and currently is a successful entrepreneur with various business interests to her name, amongst which is the successful Dreamnails Franchise and a Steers Franchise. Mogani has, since the return of her sons, Kailen and Navelan from the USA, facilitated and supported their initiatives in the establishment and management of KCF. Her business track record and acumen reinforced her input into the provisions of managerial skills and advice to both Kailen and Navelan in terms of the stated objectives of KCF, a Section 21 Company.</p>',
    ),
  );


	public function run()
	{
    if(Schema::hasTable('team_members'))
    {
      DB::table('team_members')->delete();
    }

    foreach ($this->teamMembers as $teamMember) {
			TeamMembers::create([
        'name' => $teamMember['name'],
        'title' => $teamMember['title'],
        'content' => $teamMember['content'],
			]);
		}
	}

}
