<?php
namespace Marnulombard\Kainav\Seeds;
use Illuminate\Database\Seeder;
use Eloquent;

class DatabaseSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();
//        $this->call('Marnulombard\Kainav\Seeds\ExampleUserSeeder');
//        $this->call('Marnulombard\Kainav\Seeds\ExampleSettingsSeeder');

//        $this->call('Marnulombard\Kainav\Seeds\ProjectsTableSeeder');
//        $this->call('Marnulombard\Kainav\Seeds\TeamMembersTableSeeder');
        $this->call('Marnulombard\Kainav\Seeds\SponsorsTableSeeder');
        $this->command->info('All Tables Seeded');
    }

}
