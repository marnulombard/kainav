<?php


/**
 * ======================================================================================
 * PLEASE NOTE::
 * This package may have been renamed, but it is a virtual clone of Davzie\LaravelBootstrap
 * All credit to the original Author; Not MarnuLombard
 * ======================================================================================
 */


/**
 * The application configuration file, used to setup globally used values throughout the application
 */
return array(

    /**
     * The name of the application, will be used in the main management areas of the application
     */
    'name' => 'Kainav Conservation Foundation',

    /**
     * The email address associated with support enquires on a technical basis
     */
    'support_email' => 'admin@kainavconservation.co.za',

    /**
     * The base path to put uploads into
     */
    'upload_base_path'=>'uploads/',

    /**
     * The URL key to access the main admin area
     */
    'access_url'=>'admin',

    /**
     * The menu items shown at the top and side of the application
     */
    'menu_items'=>array(
        'team_members'=>array(
            'name'=>'Team Members',
            'icon'=>'user',
            'top'=>true
        ),
        'pages'=>array(
            'name'=>'Pages',
            'icon'=>'file',
            'top'=>true
        ),
        'sliders'=>array(
            'name'=>'Sliders',
            'icon'=>'th-large',
            'top'=>true
        ),
        'projects'=>array(
            'name'=>'Research',
            'icon'=>'leaf',
            'top'=>true
        ),
        'blog'=>array(
            'name'=>'Blog',
            'icon'=>'align-center',
            'top'=>true
        ),

        'news'=>array(
            'name'=>'News',
            'icon'=>'bullhorn',
            'top'=>true
        ),
        'sponsors'=>array(
            'name'=>'Sponsors',
            'icon'=>'usd',
            'top'=>true
        ),
        'users'=>array(
            'name'=>'Users',
            'icon'=>'user',
            'top'=>false
        ),
        'settings'=>array(
            'name'=>'Settings',
            'icon'=>'cog',
            'top'=>false
        )
    )
);
