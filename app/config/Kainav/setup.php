<?php


/**
 * ======================================================================================
 * PLEASE NOTE::
 * This package may have been renamed, but it is a virtual clone of Davzie\LaravelBootstrap
 * All credit to the original Author; Not MarnuLombard
 * ======================================================================================
 */



/**
 * This configuration file is used to setup the initial user and seed to the database
 */
return array(

    'email'         =>  'admin@kainavconservation.co.za',
    'first-name'    =>  'Marnu',
    'last-name'     =>  'Lombard',
    'password'      =>  'admin',

);
