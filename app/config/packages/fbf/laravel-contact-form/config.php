<?php

return array(

	'uri' => 'contact-us',

	'view' => 'contact-us',

	'fields' => array(
		'name' => array(
      'type' => 'text',
    ),
		'title' => array(
			'type' => 'select',
			'choices' => array(
				'' => 'Please select',
				'Mr' => 'Mr',
				'Mrs' => 'Mrs',
				'Miss' => 'Miss',
				'Ms' => 'Ms',
				'Dr' => 'Dr',
				'Other' => 'Other',
			),
		),
		'email' => array(
			'type' => 'text',
		),
		'enquiry' => array(
			'type' => 'textarea',
		),
	),

	'rules' => array(
		'name' => 'required',
		'title' => 'required',
		'email' => 'required|email',
		'enquiry' => 'required',
	),

	'mail' => array(
		'views' => array(
			'laravel-contact-form::emails.html.enquiry',
			'laravel-contact-form::emails.text.enquiry',
		),
		'to' => array(
			'name' => 'Kainav Customer Service',
			'email' => 'info@kainavconservation.co.za',
		),
		'subject' => 'Website Enquiry',
	),

	'page_title' => 'Contact Us',
	'meta_description' => 'Fill in this form to contact us',
	'meta_keywords' => 'Contact',

);
