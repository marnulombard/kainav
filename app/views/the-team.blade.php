@extends('generic')

@section('custom-content')

@foreach($teamMembers as $teamMember)
  <div class="profile--wrapper">
    <div class="grid__item one-quarter">
      @foreach($teamMember->uploads as $upload)
        {{HTML::image('/uploads/'.$upload->path.'/'.$teamMember->id.'/'.$upload->filename, $teamMember->name, array('class'=>'profile__image'))}}
      @endforeach
    </div>

    <div class="grid__item three-quarters">
      <h2 class="profile--header">{{$teamMember->name}}</h2>
      <h6 class="profile--title">{{$teamMember->title}}</h6>
      {{$teamMember->content}}
    </div>
  </div>
@endforeach

@stop
