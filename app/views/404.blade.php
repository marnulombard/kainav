@extends('_layouts.default')


@section('title')
Page not Found \ KaiNav Conservation Foundation
@stop

@section('body')
<section class="grid">
  <span class="maxWidth">
    <h2>The page {{Request::path()}} is not available.</h2>
  </span>
</section>
@stop
