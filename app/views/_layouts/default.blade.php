@include('_partials.head')
<body>
  @include('_partials.header')

    @yield('body')

  @include('_partials.footer')
  @include('_partials.scripts')
  @yield('extra-scripts')
</body>
</html>
