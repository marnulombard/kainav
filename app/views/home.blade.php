@extends('_layouts.default')

@section('title')
KaiNav Conservation Foundation | Home
@stop

@section('body')

<section class="slider__index slider--wrapper">
  <ul class="slides">

    @foreach ($sliders as $slider)
    <li class="slider--item {{$slider->title}}">
      <div class="slider--text">
        <h2 class="">
          {{ $slider->content }}
        </h2>
      </div>
      @foreach($slider->uploads as $upload)
        <img src="{{$upload->sizeImg(2048, 1155, true)}}" alt="{{$slider->title}}"/>
      @endforeach
    </li>
    @endforeach

  </ul>
</section>

{{--// Sliders --}}

<section class="grey_dark thumbnails">
  <span class="maxWidth grid">

    <div class="grid__item two-thirds">
      @foreach($news as $newsItem)
        @if($newsItem->type === 'internal')
          <h2 class="heading__section">Kainav Conservation Foundation</h2>
        @elseif($newsItem->type === 'external')
          <h2 class="heading__section">News and the Natural World</h2>
        @elseif($newsItem->type === 'youtube')
          <h2 class="heading__section">From our Youtube account</h2>
        @endif

        @if($newsItem->type === 'internal' || $newsItem->type === 'external' )
          <div class="grid__item one-quarter">
            @foreach($newsItem->images as $image)
              <img src="{{$image}}" alt="{{$newsItem->title}}"/>
            @endforeach
          </div>

          <div class="grid__item three-quarters">
            <h6>{{ $newsItem->title }}</h6>
            {{ $newsItem->content }}

            @if(!is_null($newsItem->link) && $newsItem->link !== '')
              <a href="{{$newsItem->link}}" target="_blank">Read more</a> &rarr;
            @endif
          </div>

        @elseif($newsItem->type === 'youtube')
          <h6>{{ $newsItem->title }}</h6>
          <div class="center">{{ $newsItem->content }}</div>
        @endif

      @endforeach
    </div>
    <div class="grid__item one-third">
      <h2 class="heading__section">Facebook</h2>
      <div class="fb-like-box" data-href="https://facebook.com/pages/Kainav-Conservation-Foundation/1427336384169525" data-width="350" data-height="700" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true" style="background-color: white"></div>
    </div>
  </span>
</section>
{{--// News --}}


<section class="vertical-center">
  <span class="maxWidth grid">

    <h2 class="heading__section">Our Sponsors</h2>
    @foreach($sponsors as $sponsor)
    @foreach($sponsor->uploads as $upload)
      <a href="{{$sponsor->url}}" target="_blank" class="grid__item vertical-center__item one-quarter">
        {{HTML::image('/uploads/'.$upload->path.'/'.$sponsor->id.'/'.$upload->filename, $sponsor->title)}}
      </a>
    @endforeach
    @endforeach
  </span>
</section>
{{--// News --}}

@stop

@section('extra-scripts')
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=571735759611510&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
@stop
