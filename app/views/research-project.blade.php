@extends('_layouts.default')


@section('title')
KaiNav Conservation Foundation | {{$page->title}}
@stop

@section('body')
<section class="grid">

  <h1>{{$page->title}}</h1>

  <div class="section">
    @foreach($page->uploads as $upload)
      @if(!is_null($upload))
        <img src="{{$upload->sizeImg( 600 , 300 , false )}}" class="center" />
      @endif
    @endforeach
  </div>

  <div class="grid__item">{{$page->content}}</div>

</section>
@stop
