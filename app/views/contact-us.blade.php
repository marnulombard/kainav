@extends('generic')

@section('custom-content')
<section class="grid">
  <div>
  <div class="grid__item one-whole">
      <a href="{{$facebook_link}}">
        <img src="/assets/img/FB_FindUsOnFacebook-144.png" alt="Find us on facebook" class="center"/>
      </a>
  </div>
  </div>

  <div class="mt40">
  <div class="grid__item one-whole text__center">
    <p>
      Contact number: {{$contact_number}}
    </p>
  </div>
  </div>
</section>
<section class="grid contact-form" id="contact-form">
  <div class="grid__item two-thirds center">
    @include('laravel-contact-form::form')
  </div>
</section>
@stop
