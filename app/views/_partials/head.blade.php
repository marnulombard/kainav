<!doctype html>
<!--[if lt IE 7]> <html class="lt-ie10 lt-ie9 lt-ie8 lt-ie7 no-js"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie10 lt-ie9 lt-ie8 no-js"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie10 lt-ie9 no-js"> <![endif]-->
<!--[if IE 9]>    <html class="lt-ie10 no-js"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="author" content="Marnu Lombard" />
  <meta name="copyright" content="Kainav Conservation Foundation" />
  <meta name="description" content="Kainav Conservation Foundation" />
  <meta name="keywords" content="kainav, conservation, animals, csi" />

    @include('_partials.css')

  <link rel="shortcut icon" href="/favicons/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="/favicons/apple-touch-icon.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicons/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicons/apple-touch-icon-114x114.png" />

  <title>
    @yield('title')
  </title>

</head>
