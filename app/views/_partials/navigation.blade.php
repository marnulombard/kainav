<nav class="nav nav--main navbar-nav" role="navigation">
  @foreach($nav as $navLink)
    @if($navLink->type === 'link')
      <span class="nav_link">
        <a href="{{$navLink->slug}}" class="">{{$navLink->title}}</a>
      </span>
    @elseif($navLink->type === 'dropDown')
    <span class="dropdown nav_link">
    <a id="{{$navLink->slug}}" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown" class="nav_dropDown">{{$navLink->title}}</a>
      <div class="dropdown-menu" role="menu" aria-labelledby="{{$navLink->slug}}">
        @if($navLink->subLinks)
          @foreach($navLink->subLinks as $subLink)
            <a href="{{$subLink->slug}}" role="menuitem" tabindex="-1" class="nav_link--sub">{{$subLink->title}}</a>
          @endforeach
        @endif
      </div>
    </span>
    @endif
  @endforeach
</nav>
