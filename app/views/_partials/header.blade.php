<header class="header header--main section">
  <span class="maxWidth">
    <a href="/" tite="home" class="logo__header--wrapper">
      <img src="/assets/img/logo.svg" alt="KaiNav Conservation Foundation" class="logo">
    </a>
    @include('_partials.navigation')
  </span>
</header>
