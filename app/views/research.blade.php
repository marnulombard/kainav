@extends('generic')

@section('custom-content')

@foreach($projects as $project)
  <div class="project--wrapper">
    @if($project->uploads)
    <div class="grid__item one-quarter">
      @if(strlen($project->uploads) > 4)
        @foreach($project->uploads as $upload)
          {{HTML::image('/uploads/'.$upload->path.'/'.$project->id.'/'.$upload->filename, $project->name)}}
        @endforeach
      @else
        <img src="http://placehold.it/300&text=no%20image%20yet">
      @endif
    </div>
    @endif

    <div class="grid__item three-quarters">
      <h2 class="header">{{$project->title}}</h2>
      <h6 class="">{{$project->caption}}</h6>
      <a class="read-more__link" href="research-projects/{{$project->key}}">Read More</a> &rarr;
    </div>
  </div>
@endforeach

@stop
