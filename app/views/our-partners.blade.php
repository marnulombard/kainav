@extends('generic')

@section('custom-content')

@if($partners->all())
  @foreach($partners as $partner)
    <div class="sponsors_grid grid__item one-half text__center">
      @foreach($partner->uploads as $upload)
      {{HTML::image('/uploads/'.$upload->path.'/'.$partner->id.'/'.$upload->filename, $partner->name)}}
      @endforeach
      <h2 class="text__center">{{$partner->title}}</h2>
      <a href="{{$partner->url}}" target="_blank">{{$partner->url}}</a>
      {{$partner->content}}
    </div>
  @endforeach
@else
  <div class="grid__item one-whole text__center" style="background-color: gray;">
    <h4 class="mt30">None for now, <a href="{{URL::route('contact')}}">become a partner</a></h4>
  </div>
@endif

@if($sponsors->all())
<h1>Our Sponsors</h1>
@foreach($sponsors as $sponsor)
  <div class="sponsors_grid grid__item one-half text__center">
    <a href="{{$sponsor->url}}" target="_blank">
      @foreach($sponsor->uploads as $upload)
        {{HTML::image('/uploads/'.$upload->path.'/'.$sponsor->id.'/'.$upload->filename, $sponsor->name)}}
      @endforeach
    </a>
    <h5 class="text__center mt20">{{$sponsor->title}}</h5>
  </div>
@endforeach
@endif

@stop
