@extends('_layouts.default')


@section('title')
KaiNav Conservation Foundation | {{$page->title}}
@stop

@section('body')
<section class="grid">

  @if($page->launched === '1')
    <h1>{{$page->title}}</h1>
    @if(strlen($page->content > '1'))
      <div class="grid__item">{{$page->content}}</div>
    @endif
  @else
  <div class="grid__item">
    <h2>Our site is still under construction.</h2>
    <h4>Meanwhile, you can contact us below.</h4>
    @include('laravel-contact-form::form')
  </div>
  @endif

  @yield('custom-content')
</section>
@stop
