@extends('kainav::layouts.interface-edit')

@section('title')
    Edit Slider: {{ $item->title }}
@stop

@section('heading')
    <h1>Edit Slider: <small>{{ $item->title }}</small></h1>
@stop

@section('form-items')

  <div class="form-group">
    {{ Form::label( "title" , 'Slider Title' , array( 'class'=>'col-lg-2 control-label' ) ) }}
    <div class="col-lg-10">
      {{ Form::text( "title" , Input::old( "title", $item->title ) , array( 'class'=>'form-control' , 'placeholder'=>'Slider Title' ) ) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('on_page', 'On Page', array('class' => 'col-lg-2 control-label')) }}
    <select name="on_page">
      @foreach($pages as $page)
      <option value="{{$page->slug}}">{{$page->title}}</option>
      @endforeach
    </select>
  </div>
  <div class="form-group">
    {{ Form::label( "content" , 'Slider Content' , array( 'class'=>'col-lg-2 control-label' ) ) }}
    <div class="col-lg-10">
      {{ Form::textarea( "content" , Input::old( "content", $item->content ) , array( 'class'=>'form-control rich' , 'placeholder'=>'Slider Content' ) ) }}
    </div>
  </div>
    
@stop
