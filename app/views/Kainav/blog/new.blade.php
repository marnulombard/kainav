@extends('kainav::layouts.interface-new')

@section('title')
    Create New Blog Post
@stop

@section('heading')
    <h1>Create New Blog Post</h1>
@stop

@section('form-items')

    <div class="form-group">
        {{ Form::label( "title" , 'Blog Post Title' , array( 'class'=>'col-lg-2 control-label' ) ) }}
        <div class="col-lg-10">
            {{ Form::text( "title" , Input::old( "title" ) , array( 'class'=>'form-control' , 'placeholder'=>'Blog Post Title' ) ) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label( "content" , 'Blog Post Content' , array( 'class'=>'col-lg-2 control-label' ) ) }}
        <div class="col-lg-10">
            {{ Form::textarea( "content" , Input::old( "content" ) , array( 'class'=>'form-control rich' , 'placeholder'=>'Blog Post Content' ) ) }}
        </div>
    </div>
    
@stop
