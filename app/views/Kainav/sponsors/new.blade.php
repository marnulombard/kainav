@extends('kainav::layouts.interface-new')

@section('title')
    Create New Sponsor
@stop

@section('heading')
    <h1>Create New Sponsor</h1>
@stop

@section('form-items')

  <div class="form-group">
    {{ Form::label( "title" , 'Sponsor Name' , array( 'class'=>'col-lg-2 control-label' ) ) }}
    <div class="col-lg-10">
      {{ Form::text( "title" , Input::old( "title" ) , array( 'class'=>'form-control' , 'placeholder'=>'eg. Coca Cola' ) ) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label( "url" , 'Sponsor Website' , array( 'class'=>'col-lg-2 control-label' ) ) }}
    <div class="col-lg-10">
      {{ Form::text( "url" , Input::old( "url" ) , array( 'class'=>'form-control' , 'placeholder'=>'eg. cocacola.com' ) ) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label( "type" , 'Sponsor or Partner' , array( 'class'=>'col-lg-2 control-label' ) ) }}
    <div class="col-lg-10">
      {{ Form::select('type', array(
      'sponsor' => 'Sponsor',
      'partner' => 'Partner',
      ), null, array('title' => 'Choose one of the following...')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label( "content" , 'Service They Provided' , array( 'class'=>'col-lg-2 control-label' ) ) }}
    <div class="col-lg-10">
      {{ Form::textarea( "content" , Input::old( "content" ) , array( 'class'=>'form-control rich' , 'placeholder'=>'Sponsor Content' ) ) }}
    </div>
  </div>

@stop
