@extends('kainav::layouts.interface')

@section('title')
Manage Pages
@stop

@section('content')

<h1>Manage Pages</h1>

{{-- The error / success messaging partial --}}
@include('kainav::partials.messaging')

@if( !$items->isEmpty() )
<table class="table table-condensed">
  <thead>
  <tr>
    <th>Title</th>
    <th>Type</th>
    <th>Linked to</th>
    <th>Launched</th>
    <th>&nbsp;</th>
  </tr>
  </thead>
  <tbody>
  @foreach($items as $item)
  <tr>
    <td><a href="{{ $edit_url.$item->id }}">{{ $item->title }}</a></td>
    <td><a href="{{ $edit_url.$item->id }}">{{ $item->type }}</a></td>
    <td><a href="{{ $edit_url.$item->id }}">{{ $item->linked_to }}</a></td>
    <td><a href="{{ $edit_url.$item->id }}">{{ $item->launched }}</a></td>
    <td>
      <div class="pull-right">
        <a href="{{ $edit_url.$item->id }}" class="btn btn-sm btn-primary">Edit Item</a> <a href="{{ $delete_url.$item->id }}" class="btn btn-sm btn-danger">Delete Item</a>
      </div>
    </td>
  </tr>
  @endforeach
  </tbody>
</table>
@else
<div class="alert alert-info">
  <strong>No Items Yet:</strong> You don't have any items here just yet. Add one using the button below.
</div>
@endif
<a href="{{ $new_url }}" class="btn btn-primary pull-right">New Item</a>
@stop
