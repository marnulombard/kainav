@extends('kainav::layouts.interface-new')

@section('title')
Create New Page
@stop

@section('heading')
<h1>Create New Page</h1>
@stop

@section('form-items')

<div class="form-group">
  {{ Form::label( "title" , 'Page Title' , array( 'class'=>'col-lg-2 control-label' ) ) }}
  <div class="col-lg-10">
    {{ Form::text( "title" , Input::old( "title" ) , array( 'class'=>'form-control' , 'placeholder'=>'Page Title' ) ) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('type', 'Type of item', array('class' => 'col-lg-2 control-label')) }}
  {{ Form::select('type', array(
  'link' => 'Normal link',
  'dropDown' => 'Drop down',
  'subLink' => 'Sub-Link'
  ), null, array('title' => 'Choose one of the following...')) }}
</div>
<div class="form-group">
  {{ Form::label('launched', 'Is the page launched', array('class' => 'col-lg-2 control-label')) }}
  {{ Form::radio('launched', '1') }} Yes
  <br/>
  {{ Form::radio('launched', '0') }} No
</div>
<div class="form-group">
  {{ Form::label( "linked_to" , 'Linked to...' , array( 'class'=>'col-lg-2 control-label' ) ) }}
  <div class="col-lg-10">
    {{ Form::text( "linked_to" , Input::old( "linked_to" ) , array( 'class'=>'form-control' , 'placeholder'=>'linked to' ) ) }}
    <br/>
    <small>if you selected "Sub-link" in the previous option, please enter here what it should be linked to.</small>
  </div>
</div>
<div class="form-group">
  {{ Form::label( "content" , 'Page Content' , array( 'class'=>'col-lg-2 control-label' ) ) }}
  <div class="col-lg-10">
    {{ Form::textarea( "content" , Input::old( "content" ) , array( 'class'=>'form-control rich' , 'placeholder'=>'Page Content' ) ) }}
  </div>
</div>



@stop
