@extends('kainav::layouts.interface-edit')

@section('title')
    Edit Page: {{ $item->title }}
@stop

@section('heading')
    <h1>Edit Page: <small>{{ $item->title }}</small></h1>
@stop

@section('form-items')

<div class="form-group">
  {{ Form::label( "title" , 'Page Title' , array( 'class'=>'col-lg-2 control-label' ) ) }}
  <div class="col-lg-10">
    {{ Form::text( "title" , Input::old( "title", $item->title ) , array( 'class'=>'form-control' , 'placeholder'=>'Page Title' ) ) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('type', 'Type of item', array('class' => 'col-lg-2 control-label')) }}
  {{ Form::select('type', array(
  'link' => 'Normal link',
  'dropDown' => 'Drop down',
  'subLink' => 'Sub-Link'
  ), $item->type , array('title' => 'Choose one of the following...')) }}
</div>
<div class="form-group">
  {{ Form::label('launched', 'Is the page launched', array('class' => 'col-lg-2 control-label')) }}
  @if($item->launched === '1')
    {{ Form::radio('launched', '1', true) }} Yes
  @else
  {{ Form::radio('launched', '1') }} Yes
  @endif

  <br/>

  @if($item->launched === '0')
    {{ Form::radio('launched', '0', true) }} No
  @else
  {{ Form::radio('launched', '0') }} No
  @endif
</div>
<div class="form-group">
  {{ Form::label( "linked_to" , 'Linked to...' , array( 'class'=>'col-lg-2 control-label' ) ) }}
  <div class="col-lg-10">
    <small>if you selected "Sub-link" in the previous option, please enter here what it should be linked to.</small>
    <br/>
    {{ Form::text( "linked_to" , Input::old( "linked_to", $item->linked_to ) , array( 'class'=>'form-control' , 'placeholder'=>'linked to' ) ) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label( "content" , 'Page Content' , array( 'class'=>'col-lg-2 control-label' ) ) }}
  <div class="col-lg-10">
    {{ Form::textarea( "content" , Input::old( "content", $item->content ) , array( 'class'=>'form-control rich' , 'placeholder' =>'Page Content' ) ) }}
  </div>
</div>
    
@stop
