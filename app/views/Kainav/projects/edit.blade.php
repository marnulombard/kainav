@extends('kainav::layouts.interface-edit')

@section('title')
    Edit Project: {{ $item->title }}
@stop

@section('heading')
    <h1>Edit Project: <small>{{ $item->title }}</small></h1>
@stop

@section('form-items')

  <div class="form-group">
    {{ Form::label( "title" , 'Project Title' , array( 'class'=>'col-lg-2 control-label' ) ) }}
    <div class="col-lg-10">
      {{ Form::text( "title" , Input::old( "title", $item->title ) , array( 'class'=>'form-control' , 'placeholder'=>'Project Title' ) ) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label( "year" , 'Year Project Began In' , array( 'class'=>'col-lg-2 control-label' ) ) }}
    <div class="col-lg-10">
      {{ Form::text( "year" , Input::old( "year", $item->year ) , array( 'class'=>'form-control' , 'placeholder'=>'eg. 2014' ) ) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('category', 'Category', array('class' => 'col-lg-2 control-label')) }}
    {{ Form::select('category', array(
    'personal' => 'Personal',
    'programme' => 'Programme'
    ), null) }}
  </div>
  <div class="form-group">
    {{ Form::label( "caption" , 'Project’s Caption' , array( 'class'=>'col-lg-2 control-label' ) ) }}
    <div class="col-lg-10">
      {{ Form::text( "caption" , Input::old( "caption", $item->caption ) , array( 'class'=>'form-control' , 'placeholder'=>'A short description of the project' ) ) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label( "content" , 'Project Content' , array( 'class'=>'col-lg-2 control-label' ) ) }}
    <div class="col-lg-10">
      {{ Form::textarea( "content" , Input::old( "content", $item->content ) , array( 'class'=>'form-control rich' , 'placeholder'=>'Project Content' ) ) }}
    </div>
  </div>
    
@stop
