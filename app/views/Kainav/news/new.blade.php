@extends('kainav::layouts.interface-new')

@section('title')
Create New News Item
@stop

@section('heading')
<h1>Create New News Item</h1>
@stop

@section('form-items')

<div class="form-group">
  {{ Form::label( "title" , 'News Item Title' , array( 'class'=>'col-lg-2 control-label' ) ) }}
  <div class="col-lg-10">
    {{ Form::text( "title" , Input::old( "title" ) , array( 'class'=>'form-control' , 'placeholder'=>'News Item Title' ) ) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label( "type" , 'Type of News' , array( 'class'=>'col-lg-2 control-label' ) ) }}
  <div class="col-lg-10">
    {{ Form::select('type', array(
    'internal' => 'Kainav Internal News',
    'external' => 'News from the natural world',
    'youtube' => 'A new Youtube Video'
    ), null, array('title' => 'Choose one of the following...')) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label( "content" , 'News Item Content' , array( 'class'=>'col-lg-2 control-label' ) ) }}
  <div class="col-lg-10">
    {{ Form::textarea( "content" , Input::old( "content" ) , array( 'class'=>'form-control rich' , 'placeholder'=>'News Item Content' ) ) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label( "link" , 'Read More Link' , array( 'class'=>'col-lg-2 control-label' ) ) }}
  <div class="col-lg-10">
    {{ Form::text( "link" , Input::old( "link" ) , array( 'class'=>'form-control' , 'placeholder'=>'http://' ) ) }}
  </div>
</div>
@stop
