@extends('kainav::layouts.interface')

@section('content')


    @yield('heading')

    {{ Form::open( array( 'url'=>$new_url , 'class'=>'form-horizontal form-top-margin' , 'role'=>'form' ) ) }}

        {{-- The error / success messaging partial --}}
        @include('kainav::partials.messaging')

        @yield('form-items')

        {{ Form::submit('Create Item' , array('class'=>'btn btn-large btn-primary pull-right')) }}

    {{ Form::close() }}

    {{--
    @if( isset($uploadable) and $uploadable )
    <div class="col-12 col-md-4 col-lg-4">
        <h4>Upload Item Images</h4>
        <p>Drag and drop images into the box below or simply click it to select files to upload</p>
        <p><strong>Note: </strong>This will also save and refresh this page.</p>
        {{ Form::open( [ 'url' => $object_url.'/upload/'.$id , 'class' => 'dropzone square' , 'id'=>'imageUploads' , 'files'=>true ] ) }}
        <div class="fallback">
          <input name="file" type="file" multiple />
        </div>
        {{ Form::close() }}
    </div>
    @endif
    --}}

@stop
