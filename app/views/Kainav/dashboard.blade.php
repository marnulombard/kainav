@extends('kainav::layouts.interface')

@section('title')
    {{ $app_name }}
@stop

@section('content')

    <h1>Welcome To {{ $app_name }}</h1>
    <p>Manage content for your website, including but not limited to:</p>
    <ul>
      <li>Pages</li>
      <li>Blog Posts</li>
      <li>Research Items</li>
      <li>Projects</li>
    </ul>

@stop
