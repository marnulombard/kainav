<?php

require __DIR__.'/bindings.php'; // the IoC bindings


//-----------------------------------------------------------------
// Front-end Routes
//=================================================================

// Home Page
Route::get('/', 'Marnulombard\Kainav\Frontend\Controllers\FrontEndPagesController@getHome');
Route::get('home', 'Marnulombard\Kainav\Frontend\Controllers\FrontEndPagesController@getHome');


// About Us Routes
//-----------------------------------------------------------------
// The Team
Route::get('the-team', 'Marnulombard\Kainav\Frontend\Controllers\FrontEndPagesController@getTheTeam');


// Research Routes
//-----------------------------------------------------------------
Route::get('research-projects', 'Marnulombard\Kainav\Frontend\Controllers\FrontEndPagesController@getIndividualResearch');
Route::get('research-projects/{project}', 'Marnulombard\Kainav\Frontend\Controllers\FrontEndPagesController@getResearchProject');


// Our Partners
//-----------------------------------------------------------------
Route::get('our-partners', 'Marnulombard\Kainav\Frontend\Controllers\FrontEndPagesController@getOurPartners');


// Contact-us Route
//-----------------------------------------------------------------
Route::get('contact-us',
  [
    'as' => 'contact',
    'uses' => 'Marnulombard\Kainav\Frontend\Controllers\FrontEndPagesController@getContact',
  ]
);




// Other Pages
Route::get('{slug}', 'Marnulombard\Kainav\Frontend\Controllers\FrontEndPagesController@getPage');


// 404 Page
App::missing(function($exception) {
  return Response::view('404', array(), 404);
});

//=================================================================
