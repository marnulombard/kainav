'use strict';
module.exports = function(grunt) {

    // load all grunt tasks matching the `grunt-*` pattern
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        // watch for changes and trigger sass, concat, uglify and livereload
        watch: {
            svgmin: {
                files: ['img/*.svg'],
                tasks: ['svgmin']
            },
            icons: {
                files: ['icons/*.svg'],
                tasks: ['webfont']
            },
            sass: {
                files: ['scss/**/*', 'scss/*'],
                tasks: ['sass:dev']
            },
            js: {
                files: [
                    'Gruntfile.js',
                    'js/*',
                    'js/**/*'
                ],
                tasks: ['concat']
            },
            livereload: {
                options: { livereload: true },
                files: [
                    '../public/assets/css/*',
                    '../public/assets/js/*',
                    '../public/assets/*.php',
                    '../public/assets/includes/**/*.php'
                ]
            }
        }, // end watch

        // Compress all svg files
        svgmin: {
            options: {
                plugins: [{
                    removeViewBox: false
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'img',
                    src: ['**/*.svg'],
                    dest: '../public/assets/img/',
                    ext: '.svg'
                }]
            }
        },

        // To generate the icon fonts from the files in my ./src/icons/ directory
        webfont: {
            icons: {
                src: "icons/*.svg",
                dest: "../public/assets/fonts",
                options: {
                    hashes: false,
                    htmlDemo: false,
                    stylesheet: "scss"
                }
            }
        },

        // sass and scss
        sass: {
            dev: {
                options: {
                    sourcemap: true,
                    style: 'nested',
                    precision: '2',
                    compass: true,
                    cache: 'delete/'
                },
                files: {
                    '../public/assets/css/style.css':'scss/style.scss',
                    '../public/assets/css/no-mq.css':'scss/no-mq.scss'
                }
            },
            dist: {
                options: {
                    sourcemap: false,
                    style: 'compressed',
                    precision: '2',
                    compass: true,
                    require: 'sass-globbing',
                    cache: 'delete/'
                },
                files: {
                    '../public/assets/css/style.css':'scss/style.scss',
                    '../public/assets/css/no-mq.css':'scss/no-mq.scss'
                }
            }
        },

        // concat files
        concat: {
            dev : {
                files: {
                    '../public/assets/js/script.min.js': [
                        'js/vendor/*',
                        'js/plugins/*',
                        'js/app.js'
                    ]
                }
            },
            dist: {
                // to avoid ie errors on uglify
                files: {
                    '../public/assets/js/ie.min.js': [
                        'js/ie/*'
                    ]
                }
            }
        },

        // uglify to concat & minify
        uglify: {
            dist: {
                files: {
                    '../public/assets/js/script.min.js': [
                        'js/vendor/*',
                        'js/plugins/*',
                        'js/app.js'
                    ]
                }
            }
        }

    });

    // register task
    grunt.registerTask('dev', ['watch']);
    grunt.registerTask('dist', ['webfont', 'sass:dist', 'concat:dist', 'uglify:dist']);

};
